import React from "react";

import { Menu } from "antd";
import { itemsSidebar } from "./SideMenuData";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
const SideMenu = ({ isCollapsed }) => {
  const location = useLocation();
  const navigate = useNavigate();

   const handleMenuClick = ({ key }) => {
    if (key) {
      navigate(key);
    }
  };

  return (
    <Menu
      defaultSelectedKeys={[location.pathname]}
      defaultOpenKeys={[location.pathname]}
      mode="inline"
      theme="light"
      items={itemsSidebar}
      className={"mb-6"}
      onClick={handleMenuClick}
    />
  );
};

export default SideMenu;
