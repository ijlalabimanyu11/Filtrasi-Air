import React, { useEffect, useState } from "react";
import { FileExcelFilled } from "@ant-design/icons";
import { Excel } from "antd-table-saveas-excel";
import { Button, notification } from "antd";

const XLSX = (props) => {
  const { data, columns } = props;

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const exportToExcel = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      notification.success({
        message: "Download Successful",
        description: "Your data has been downloaded.",
      });
      setConfirmLoading(false);
    }, 2000);

    const excel = new Excel();
    const date = Date().split(" ");
    const dateStr = date[2] + "-" + date[1] + "-" + date[3];
    const fileName = `Filtration Data (${dateStr})`;
    excel
      .addSheet(`Filtration Data (${dateStr})`)
      .addColumns(columns)
      .addDataSource(data, {
        str2Percent: true,
      })
      .saveAs(fileName + ".xlsx");
  };

  return (
    <div>
      <Button
        onClick={exportToExcel
        }
        disabled={data !== undefined ? false : true}
        className={` ${
          data !== undefined ? "" : "cursor-not-allowed"
        }ml-2 inline-flex justify-center items-center rounded-lg bg-[#FE634E] px-5 py-5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"`}
        icon={
          <FileExcelFilled
            style={{ fontSize: "16px" }}
            className="fill-current text-white text-lg"
          />
        }
      >
        Download Excel
      </Button>
    </div>
  );
};

export default XLSX;
