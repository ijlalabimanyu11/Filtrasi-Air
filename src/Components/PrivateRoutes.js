import React from "react";
import { Navigate, Outlet, useLocation, Route } from "react-router-dom";
import { useSelector } from "react-redux";

const PrivateRoute = () => {
  const { isLoggedIn } = useSelector((state) => state.auth);
  const token = localStorage.getItem('token') || window.sessionStorage.getItem('token')
  
  return (token ?
      <Outlet />
      :
      <Navigate to={"/"} />);
}

export default PrivateRoute;

