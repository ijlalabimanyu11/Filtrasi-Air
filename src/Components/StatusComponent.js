import React from "react";

const StatusComponent = (props) => {
  const { color, children } = props;
  let bg;

  switch (color) {
    case "VERY GOOD":
      return (
        <div>
          <p
            className={`text-white font-semibold bg-[#14A38B] px-3 py-[3px] rounded-[16px] text-center justify-center`}
          >
            {children}
          </p>
        </div>
      );
    case "GOOD":
      return (
        <div>
          <p
            className={`text-white font-semibold bg-[#0075BF] px-3 py-[3px] rounded-[16px] text-center justify-center`}
          >
            {children}
          </p>
        </div>
      );
    case "BAD":
      return (
        <div>
          <p
            className={`text-white font-semibold bg-[#FF6B00] px-3 py-[3px] rounded-[16px] text-center justify-center`}
          >
            {children}
          </p>
        </div>
      );
    case "VERY BAD":
      return (
        <div>
          <p
            className={`text-white font-semibold bg-[#D85442] px-3 py-[3px] rounded-[16px] text-center justify-center`}
          >
            {children}
          </p>
        </div>
      );
    case "WARNING":
      return (
        <div>
          <p
            className={`text-white font-semibold bg-[#BE3036] px-3 py-[3px] rounded-[16px] text-center justify-center`}
          >
            {children}
          </p>
        </div>
      );
    default:
      break;
  }

  return (
    <div>
      <p
        className={`text-white font-semibold ${bg} px-3 py-[3px] rounded-[16px] text-center justify-center`}
      >
        {children}
      </p>
    </div>
  );
};
export default StatusComponent;
