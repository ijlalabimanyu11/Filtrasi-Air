import {
  MdDashboard,
  MdOutlineSettingsRemote,
  MdOutlineWaterDrop,
} from "react-icons/md";
import { RiTimerLine } from "react-icons/ri";
import { SiMoleculer } from "react-icons/si";
import { FaThermometerEmpty } from "react-icons/fa";
import { IoDocumentTextOutline } from "react-icons/io5";

export const itemsSidebar = [
  {
    label: "Dashboard",
    icon: <MdDashboard size={16} />,
    key: "/app/dashboard",
  },
  {
    label: "Real Time",
    icon: <RiTimerLine size={16} />,
    key: "/app/real-time",
  },
  {
    label: "Total Dissolve Solid",
    icon: <MdOutlineSettingsRemote size={16} />,
    key: "/app/total-dissolve-solid",
  },
  {
    label: "Acidity",
    icon: <MdOutlineWaterDrop size={16} />,
    key: "/app/acidity",
  },
  {
    label: "Turbidity",
    icon: <SiMoleculer size={16} />,
    key: "/app/turbidity",
  },
  {
    label: "Temperature",
    icon: <FaThermometerEmpty size={16} />,
    key: "/app/temperature",
  },
  // {
  //   label: "Report",
  //   icon: <IoDocumentTextOutline size={16} />,
  //   key: "/app/report",
  // },
];
