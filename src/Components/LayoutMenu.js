import React, { useState } from "react";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  LogoutOutlined,
  WarningOutlined,
} from "@ant-design/icons";
import { Layout, Button, Modal, Image, Space, Tooltip } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import SideMenu from "./SideMenu";
import { LogoFDL, LogoKecil } from "../Assets/Image";
import { logout } from "../Redux/slices/Auth";

const { Content, Sider, Header } = Layout;

const LayoutMenu = ({ children, header }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [collapsed, setCollapsed] = useState(false);
  const [modalConfirmation, setModalConfirmation] = useState(false);

  // handle logout
  const handleLogoutModal = () => {
    setModalConfirmation(true);
  };

  const handleCancel = () => {
    setModalConfirmation(false);
  };

  const handleLogout =  () => {
    try {
      dispatch(logout());
      localStorage.removeItem("token");
      return navigate("/");
    } catch (error) {
    }
  };

  return (
    <>
      <Layout
        style={{
          height: "100%",
        }}
        className="site-layout"
      >
        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
          className={"site-layout-background"}
          style={{
            overflow: "auto",
          }}
        >
          <div
            className={`grid grid-cols-2 gap-1 logo ${
              collapsed
                ? "my-6 mx-4 justify-center"
                : "my-6 mx-4 justify-center"
            }`}
          >
            <div className="col-span-2">
              <Image
                src={collapsed ? LogoKecil : LogoFDL}
                preview={false}
                // className="py-1 px-1"
              />
            </div>
            {/* <div className="flex self-center justify-end">
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                  style: {
                    fontSize: "24px",
                    color: "#4B465C",
                    width: "24px",
                  },
                }
              )}
            </div> */}
          </div>
          <SideMenu isCollapsed={collapsed} />
        </Sider>

        <Layout className="site-layout2">
          <Header className="flex justify-between site-layout-background2 my-4 mx-10 rounded-lg drop-shadow-lg">
            <div className="flex self-center justify-end">
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                  style: {
                    fontSize: "24px",
                    color: "#4B465C",
                    width: "24px",
                  },
                }
              )}
            </div>
            <span className="font-bold text-[28px] text-bold tracking-wide">
              {header}
            </span>
            <Space className="flex justify-end ">
              <Tooltip title="Log Out">
                <LogoutOutlined
                  className="flex items-center p-1 my-4 bg-[#FE634E]/20 rounded-full"
                  style={{
                    fontSize: "24px",
                    color: "#FE634E",
                  }}
                  onClick={() => {
                    handleLogoutModal();
                  }}
                />
              </Tooltip>
            </Space>
          </Header>

          <Content
            style={{
              margin: "20px 20px",
            }}
          >
            {children}
          </Content>

          <Modal
            open={modalConfirmation}
            onCancel={handleCancel}
            title={"Logout"}
            onOk={handleLogout}
            centered
            width={500}
            footer={[
              <Button
                key="back"
                onClick={handleCancel}
                className="inline-flex items-center justify-center rounded-md border-[#FE634E] bg-white px-3 py-1.5 text-sm font-semibold text-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
              >
                Cancel
              </Button>,
              <Button
                key="submit"
                onClick={handleLogout}
                className="inline-flex items-center justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
              >
                OK
              </Button>,
            ]}
          >
            <div className={"flex flex-col px-2 py-2"}>
              <div className="w-full inline-flex items-center justify-center">
                <WarningOutlined
                  style={{ fontSize: "36px", color: "#BE3036" }}
                />
                <span className={"pl-6 text-xl text-[#000000]"}>
                  {" "}
                  Are you sure want to log out ?
                </span>
              </div>
            </div>
          </Modal>
        </Layout>
      </Layout>
    </>
  );
};

export default LayoutMenu;
