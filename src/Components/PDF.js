import React, { useState } from "react";
import jsPDF from "jspdf";
import "jspdf-autotable";
import { Button, Select, notification, Modal } from "antd";
import { FilePdfFilled } from "@ant-design/icons";
import { LogoFDL } from "../Assets/Image";

const { Option } = Select;

const PDF = (props) => {
  const { data, columns } = props;

  const [paper, setPaper] = useState("");
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [preview, setPreview] = useState("");
  const [modal, setModal] = useState(false);

  const exportToPDF = (param, orientation) => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);

    const doc = new jsPDF({
      orientation: orientation,
      unit: "pt",
      format: "a4",
    });

    const date = Date().split(" ");
    const dateStr = date[2] + "-" + date[1] + "-" + date[3];

    doc.autoTable(columns, data, {
      startY: 50,
      margin: { top: 60, right: 10, bottom: 25, left: 20 },
      theme: "striped",
      styles: {
        font: "times",
        // halign: "center",
        cellPadding: 3.5,
        lineWidth: 0.5,
        lineColor: [0, 0, 0],
        textColor: [0, 0, 0],
      },
      headStyles: {
        cellWidth: "wrap",
        textColor: [0, 0, 0],
        fontStyle: "normal",
        lineWidth: 0.5,
        lineColor: [0, 0, 0],
        fillColor: [166, 204, 247],
      },
      didDrawPage: function (data) {
        const image = new Image();

        // Header
        doc.setFontSize(16);
        doc.setTextColor("#161C22");
        doc.text("Filtration Data", data.settings.margin.left, 35);
        doc.addImage(
          LogoFDL,
          "PNG",
          orientation == "p" ? 530 : 775,
          10,
          55,
          30
        );

        // Footer
        let str = "" + doc.internal.getNumberOfPages();
        doc.setFontSize(10);
        let pageSize = doc.internal.pageSize;
        let pageHeight = pageSize.height
          ? pageSize.height
          : pageSize.getHeight();
        doc.text(dateStr, data.settings.margin.left, pageHeight - 10);
        doc.text(orientation == "p" ? 550 : 795, pageHeight - 10, str);
      },
    });

    const fileName = `Filtration Data (${dateStr}).pdf`;
    const tempTitle = document.title;
    document.title = fileName;
    document.title = tempTitle;

    doc.setProperties({
      title: fileName,
    });
    if (param) {
      doc.save(`Filtration Data (${dateStr}).pdf`);
      notification.success({
        message: "Download Successful",
        description: "Your data has been downloaded.",
      });
      setModal(false);
    } else {
      setPreview(doc.output("bloburl"));
      setModal(true);
    }
  };

  return (
    <div>
      <Button
        onClick={() => exportToPDF(false, "p")}
        disabled={data !== undefined ? false : true}
        className={` ${
          data !== undefined ? "" : "cursor-not-allowed"
        }ml-2 inline-flex justify-center items-center rounded-lg bg-[#FE634E] px-5 py-5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"`}
        icon={
          <FilePdfFilled
            style={{ fontSize: "16px" }}
            className="fill-current text-white text-lg"
          />
        }
      >
        Download PDF
      </Button>
      <Modal
        title={`Filtration Report Preview`}
        open={!!modal}
        className=" h-full"
        onCancel={() => setModal(false)}
        closable
        centered
        footer={[
          <Button
            onClick={() => setModal(false)}
            className="inline-flex items-center justify-center rounded-md border-[#FE634E] bg-white px-3 py-1.5 text-sm font-semibold text-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
          >
            Cancel
          </Button>,
          <Button
            onClick={() => {
              exportToPDF(true, paper);
            }}
            className="inline-flex items-center justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
          >
            OK
          </Button>,
        ]}
      >
        <div className=" mt-5 flex justify-start ">
          <Select
            placeholder="Select"
            className="w-32"
            onChange={(e) => {
              exportToPDF(false, e);
              setPaper(e);
            }}
          >
            <Option value="p">Portrait</Option>
            <Option value="l">Landscape</Option>
          </Select>
        </div>
        <div className=" h-[70vh]">
          <iframe
            title="preview"
            className=" w-full h-full my-5"
            src={preview}
            frameBorder="0"
          ></iframe>
        </div>
      </Modal>
    </div>
  );
};

export default PDF;
