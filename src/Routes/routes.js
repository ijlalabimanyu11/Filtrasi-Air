import React from 'react'
import Dashboard from '../App/Pages/Dashboard';
import RealTime from '../App/Pages/RealTime';
import TDS from '../App/Pages/TDS';
import Turbidity from '../App/Pages/Turbidity';
import Acidity from '../App/Pages/Acidity'
import Temperature from '../App/Pages/Temperature';
import Report from '../App/Pages/Report';

export const SidebarData = [
	{
		path: "/app/dashboard",
		element: <Dashboard />,
	},
	{
		path: "/app/real-time",
		element: <RealTime />,
	},
	{
		path: "/app/total-dissolve-solid",
		element: <TDS />,
	},
	{
		path: "/app/acidity",
		element: <Acidity />,
	},
	{
		path: "/app/turbidity",
		element: <Turbidity />,
	},
	{
		path: "/app/temperature",
		element: <Temperature />,
	},
	{
		path: "/app/report",
		element: <Report />,
	},
];
