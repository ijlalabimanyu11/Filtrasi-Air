import React from "react";
import { Routes, Route } from "react-router-dom";
import ForgotPassword from "../App/Authentication/ForgotPassword";
import LogIn from "../App/Authentication/LogIn";
import NewPassword from "../App/Authentication/NewPassword";
import Register from "../App/Authentication/Register";
import NotFound from "../App/NotFound";
import PrivateRoutes from "../Components/PrivateRoutes";
import { SidebarData } from "./routes";
import { AnimatePresence, motion } from "framer-motion";

const pageVariants = {
  initial: {
    opacity: 1,
  },
  in: {
    opacity: 1,
  },
  out: {
    opacity: 2,
  },
};

const pageTransition = {
  type: "tween",
  ease: "linear",
  duration: 4,
};

const AppRoutes = () => {
  return (
    <AnimatePresence mode="wait">
      <Routes>
        {/* Public Route */}
        <Route path="/" element={<LogIn />} />
        <Route path="/register" element={<Register />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/new-password" element={<NewPassword />} />

        {/* Protected Route */}
        <Route element={<PrivateRoutes />}>
          {SidebarData.map((menus, key) => {
            return (
              <Route
                exact
                path={menus.path}
                key={key}
                element={menus.element}
              />
            );
          })}
        </Route>

        {/* Not Found  */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </AnimatePresence>
    // <motion.div
    //   initial="initial"
    //   animate="in"
    //   variants={pageVariants}
    //   transition={pageTransition}
    //   exit={{ opacity: 0 }}
    // >
    //   <Routes>
    //     {/* Public Route */}
    //     <Route path="/" element={<LogIn />} />
    //     <Route path="/register" element={<Register />} />
    //     <Route path="/forgot-password" element={<ForgotPassword />} />
    //     <Route path="/new-password" element={<NewPassword />} />

    //     {/* Protected Route */}
    //     <Route element={<PrivateRoutes />}>
    //       {SidebarData.map((menus, key) => {
    //         return (
    //           <Route
    //             exact
    //             path={menus.path}
    //             key={key}
    //             element={menus.element}
    //           />
    //         );
    //       })}
    //     </Route>

    //     {/* Not Found  */}
    //     <Route path="*" element={<NotFound />} />
    //   </Routes>
    // </motion.div>
  );
};

export default AppRoutes;
