const BASE_URL = process.env.REACT_APP_FILTRASI_SERVER;

export const ConfigApp = {
  SERVICE: `${BASE_URL}`,
};
