export const tokenHeader = () => {
	const token =
		JSON.parse(localStorage.getItem("token")) ||
		JSON.parse(window.sessionStorage.getItem("token"));
	// return  { authorization: token };
	return {authorization: token, 'Content-Type' : 'application/json'}
};

