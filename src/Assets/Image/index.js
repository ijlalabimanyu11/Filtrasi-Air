import Turbidity1 from "./Turbidity(1).png"
import Turbidity2 from "./Turbidity(2).png"
import Turbidity3 from "./Turbidity(3).png"
import Temperature1 from "./Temperature(1).png"
import Temperature2 from "./Temperature(2).png"
import Temperature3 from "./Temperature(3).png"
import LogoFDL from "./LogoFDL.png"
import LogoKecil from "./LogoKecil.png"
import BgLogin from "./BgLogin.png"
import BgForgot from "./ForgotPassword.png"
import BgNewPass from "./NewPassword.png"
import BgRegister from "./Register.png"
import TDS1 from "./TDS(1).png"
import TDS2 from "./TDS(2).png"
import TDS3 from "./TDS(3).png"
import Acidity1 from "./Acidity(1).png"
import Acidity2 from "./Acidity(2).png"
import Acidity3 from "./Acidity(3).png"


export {
    Turbidity1,
    Turbidity2,
    Turbidity3,
    Temperature1,
    Temperature2,
    Temperature3,
    LogoFDL,
    LogoKecil,
    BgLogin,
    BgForgot,
    BgNewPass,
    BgRegister,
    TDS1,
    TDS2,
    TDS3,
    Acidity1,
    Acidity2,
    Acidity3
}