import React from "react";

const SVG = ({ style = {}, width = "100%", className = "", onClick = {} }) => (
  <svg
    width={width}
    style={style}
    height={width}
    className={`cursor-pointer${className}`}
    onClick={onClick}
    viewBox="0 0 23 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M17.9167 7.98416L13.0281 4.18183C12.1456 3.49522 10.9095 3.49522 10.027 4.18183L5.13746 7.98416C4.542 8.44723 4.19386 9.15941 4.19421 9.91374V16.5137C4.19421 17.5263 5.01503 18.3471 6.02755 18.3471H17.0275C18.0401 18.3471 18.8609 17.5263 18.8609 16.5137V9.91374C18.8609 9.15933 18.5125 8.44708 17.9167 7.98416"
      stroke="#FE634E"
      stroke-width="1.75"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
    <path
      d="M15.1666 13.7499C13.1408 14.9718 9.85731 14.9718 7.83331 13.7499"
      stroke="#FE634E"
      stroke-width="1.75"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </svg>
);

export default SVG;
