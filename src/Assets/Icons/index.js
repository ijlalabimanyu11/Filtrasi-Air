import React from "react";
import { IconDashboard } from "./IconDashboard";

const Icon = (props) => {
  switch (props.name) {
    case "IconHierarchy":
      return <IconDashboard {...props} />;
    default:
      return;
  }
};

export default Icon;
