import React, { useState } from "react";
import { Checkbox, Form, Input, Card, Button, Modal, Spin } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  CloseCircleOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { BgLogin, LogoFDL } from "../../Assets/Image";
import { login } from "../../Redux/slices/Auth";
import Transitions from "../../Components/Transitions";

const LogIn = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { loading } = useSelector((state) => state.auth);

  const [openModal, setOpenModal] = useState(false);

  const handleLogin = (formValue) => {
    form.resetFields();

    let data;
    data = {
      username: formValue.username,
      password: formValue.password,
    };

    dispatch(login({ user: data, remember: formValue.remember }))
      .unwrap()
      .then(() => {
        navigate("app/dashboard");
      })
      .catch(() => {
        setOpenModal(true);
        console.log("1");
      });
  };

  return (
    <Transitions>
      <Spin spinning={loading}>
        <div className="w-screen h-screen grid grid-cols-12 dark:bg-dark-blue">
          <div className="col-span-12 flex bg-no-repeat bg-cover w-full justify-end items-center">
            <div
              className={"w-screen h-full bg-[#FFDAD5]"}
              style={{
                backgroundImage: `url(${BgLogin})`,
              }}
            ></div>
            <Card style={{ width: "70%", height: "100%" }}>
              {/* header card */}
              <div className={"flex flex-col gap-8 "}>
                <img
                  className="mx-auto mt-7 w-[200px] h-[80px]"
                  src={LogoFDL}
                  alt="Your Company"
                />
                <h2 className="text-center text-base tracking-tight dark:text-[#FE634E]">
                  Please login with your registered account
                </h2>
              </div>
              {/* form */}
              <div
                className={
                  "flex flex-col justify-center items-center w-full px-11 mt-[3.62rem]"
                }
              >
                <Form
                  form={form}
                  name="basic"
                  layout="vertical"
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={handleLogin}
                  className={"w-full"}
                  autoComplete="off"
                >
                  <Form.Item
                    label={
                      <span className="font-regular text-sm tracking-wide dark:text-[#000000]">
                        Username
                      </span>
                    }
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: "Please input your username!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input
                      size="large"
                      placeholder="Username"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item
                    label={
                      <span className="font-regular text-center text-sm tracking-wide dark:text-[#000000]">
                        Password
                      </span>
                    }
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input.Password
                      size="large"
                      placeholder="Password"
                      iconRender={(visible) =>
                        visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                      }
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item name={"remember"} valuePropName={"Unchecked"}>
                    <Checkbox> Remember Me</Checkbox>
                  </Form.Item>
                  <div
                    className={
                      "w-full flex flex-col items-center justify-center mt-5"
                    }
                  >
                    <Form.Item className={"w-full"}>
                      <Button
                        // type="primary"
                        htmlType="submit"
                        className="w-full flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                      >
                        Login
                      </Button>
                    </Form.Item>
                    <Link
                      to="/forgot-password"
                      className=" dark:text-[#FE634E] cursor-pointer mb-7"
                    >
                      Forgot Password
                    </Link>
                  </div>
                </Form>
              </div>
              <span className="dark:text-[#FE634E] text-[18px] ml-12 flex justify-start mb-8">
                Don't have an account yet?{" "}
                <Link
                  to="/register"
                  className=" dark:text-[#FE634E] cursor-pointer underline italic"
                >
                  Register
                </Link>
              </span>

              {/* footer card*/}
              <span className="dark:text-[#FE634E] text-[10px] flex justify-center mb-8">
                Copyrights © 2023 Filtration Detergent Laundry. All rights
                reserved
              </span>
            </Card>
          </div>

          {openModal && (
            <Modal
              open={openModal}
              onCancel={() => setOpenModal(false)}
              footer={false}
              centered={true}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CloseCircleOutlined
                    style={{ fontSize: "32px", color: "#FF0000" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Oops, login failed...
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        Username or Password is incorrect!
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    type={"submit"}
                    onClick={() => setOpenModal(false)}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </div>
            </Modal>
          )}
        </div>
      </Spin>
    </Transitions>
  );
};

export default LogIn;
