import React, { useState } from "react";
import { Form, Input, Card, Button, Modal, Spin } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import { BgRegister, LogoFDL } from "../../Assets/Image";
import { registerSlice } from "../../Redux/slices/Users";
import Transitions from "../../Components/Transitions";

const Register = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { loading } = useSelector((state) => state.user);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);

  const handleLogin = (formValue) => {
    form.resetFields();

    let data;
    data = {
      username: formValue.username,
      email: formValue.email,
      password: formValue.password,
      confirm_password: formValue.confirm_password,
    };

    dispatch(registerSlice(data))
      .unwrap()
      .then(() => {
        setModalSuccess(true);
      })
      .catch(() => {
        setIsModalOpen(true);
        console.log("1");
      });
  };

  return (
    <Transitions>
      <Spin spinning={loading}>
        <div className="w-screen h-screen grid grid-cols-12 dark:bg-dark-blue">
          <div className="col-span-12 flex bg-no-repeat bg-cover w-full justify-end items-center">
            <div
              className={"w-screen h-full bg-[#FFDAD5]"}
              style={{
                backgroundImage: `url(${BgRegister})`,
              }}
            ></div>
            <Card style={{ width: "70%", height: "100%" }}>
              {/* header card */}
              <div className={"flex flex-col gap-8 "}>
                <img
                  className="mx-auto mt-7 w-[200px] h-[80px]"
                  src={LogoFDL}
                  alt="Your Company"
                />
                <h2 className="text-center text-base tracking-tight dark:text-[#FE634E]">
                  Register your account
                </h2>
              </div>

              {/* form */}
              <div
                className={
                  "flex flex-col justify-center items-center w-full px-11 mt-[3.62rem]"
                }
              >
                <Form
                  name="basic"
                  layout="vertical"
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={handleLogin}
                  className={"w-full"}
                  autoComplete="off"
                  form={form}
                >
                  <Form.Item
                    label={
                      <span className="font-regular text-sm text-base tracking-wide dark:text-[#000000]">
                        Username
                      </span>
                    }
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: "Please input your username!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input
                      type="username"
                      size="large"
                      placeholder="Username"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item
                    label={
                      <span className="font-regular text-sm text-base tracking-wide dark:text-[#000000]">
                        Email
                      </span>
                    }
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: "Please input your email!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input
                      type="email"
                      size="large"
                      placeholder="hello@example.com"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    hasFeedback
                    label={
                      <span className="font-regular text-center text-sm tracking-wide dark:text-[#000000]">
                        Password
                      </span>
                    }
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input.Password
                      type="password"
                      size="large"
                      placeholder="Password"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item
                    name="confirm_password"
                    dependencies={["password"]}
                    hasFeedback
                    label={
                      <span className="font-regular text-center text-sm tracking-wide dark:text-[#000000]">
                        Confirm Password
                      </span>
                    }
                    rules={[
                      {
                        required: true,
                        message: "Please input your confirm password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The new password that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                    className="mb-8"
                  >
                    <Input.Password
                      type="confirm_password"
                      size="large"
                      placeholder="Confirm Password"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <div
                    className={
                      "w-full flex flex-col items-center justify-center mt-5"
                    }
                  >
                    <Form.Item className={"w-full"}>
                      <Button
                        // type="primary"
                        htmlType="submit"
                        className="w-full flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                      >
                        Submit
                      </Button>
                    </Form.Item>
                  </div>
                </Form>
              </div>
              <span className="dark:text-[#FE634E] text-[18px] ml-12 flex justify-start mb-8">
                Already have an account?
                <Link
                  to="/"
                  className=" dark:text-[#FE634E] cursor-pointer underline italic"
                >
                  Login
                </Link>
              </span>

              {/* footer card*/}
              <span className="dark:text-[#FE634E] text-[10px] flex justify-center mb-8">
                Copyrights © 2023 Filtration Detergent Laundry. All rights
                reserved
              </span>
            </Card>
          </div>

          {isModalOpen && (
            <Modal
              open={isModalOpen}
              onCancel={() => setIsModalOpen(false)}
              footer={false}
              centered={true}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CloseCircleOutlined
                    style={{ fontSize: "32px", color: "#FF0000" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Oops, login failed...
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        This username or email is already in use
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    type={"submit"}
                    onClick={() => setIsModalOpen(false)}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </div>
            </Modal>
          )}

          {modalSuccess && (
            <Modal
              open={modalSuccess}
              onCancel={() => setModalSuccess(false)}
              footer={false}
              centered={true}
              width={600}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CheckCircleOutlined
                    style={{ fontSize: "32px", color: "#A4BE37" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Success
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        Congratulations! Your account has been successfully
                        created.
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    onClick={() => {
                      navigate("/");
                      window.location.reload();
                    }}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    OK
                  </Button>
                </div>
              </div>
            </Modal>
          )}
        </div>
      </Spin>
    </Transitions>
  );
};

export default Register;
