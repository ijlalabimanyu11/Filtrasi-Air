import { Button, Card, Form, Input, Spin, Modal } from "antd";
import React, { useState } from "react";
import { BgNewPass, LogoFDL } from "../../Assets/Image";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { newPasswordSlice } from "../../Redux/slices/Users";
import Transitions from "../../Components/Transitions";

const NewPassword = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { loading } = useSelector((state) => state.user);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);

  const handleLogin = (formValue) => {
    form.resetFields();

    let data;
    data = {
      username: formValue.username,
      password: formValue.password,
      confirm_password: formValue.confirm_password,
    };

    dispatch(newPasswordSlice(data))
      .unwrap()
      .then(() => {
        setModalSuccess(true);
      })
      .catch(() => {
        setIsModalOpen(true);
        console.log("1");
      });
  };

  return (
    <Transitions>
      <Spin spinning={loading}>
        <div className="w-screen h-screen grid grid-cols-12">
          <div className="col-span-12 flex bg-no-repeat bg-cover w-full justify-end items-center">
            <div
              className={"w-screen h-full bg-[#FFDAD5]"}
              style={{
                backgroundImage: `url(${BgNewPass})`,
              }}
            ></div>
            <Card style={{ width: "70%", height: "100%" }}>
              <div className={"flex flex-col gap-8 "}>
                <img
                  className="mx-auto mt-7 w-[200px] h-[80px]"
                  src={LogoFDL}
                  alt="Your Company"
                />
                <h2 className="text-center text-base tracking-tight dark:text-[#FE634E]">
                  Forgot Password
                </h2>
              </div>

              {/* Form */}

              <div
                className={
                  "flex flex-col justify-center items-center w-full px-11 mt-[3.62rem]"
                }
              >
                <Form
                  name="basic"
                  layout="vertical"
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={handleLogin}
                  className={"w-full"}
                  autoComplete="off"
                  form={form}
                >
                  <Form.Item
                    label={
                      <span className="font-regular text-sm text-base tracking-wide dark:text-[#000000]">
                        Username
                      </span>
                    }
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: "Please input your username!",
                      },
                    ]}
                    className="mb-8"
                  >
                    <Input
                      type="username"
                      size="large"
                      placeholder="Username"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      type="password"
                      size="large"
                      placeholder="Password"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <Form.Item
                    name="confirm_password"
                    label="Confirm Password"
                    dependencies={["password"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Please input your confirm password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The new password that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      type="confirm_password"
                      size="large"
                      placeholder="Confirm Password"
                      className="mb-2 !text-base"
                    />
                  </Form.Item>

                  <div
                    className={
                      "w-full flex flex-col items-center justify-center mt-5"
                    }
                  >
                    <Form.Item className={"w-full"}>
                      <Button
                        // type="primary"
                        htmlType="submit"
                        className="w-full flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                      >
                        Submit
                      </Button>
                    </Form.Item>
                  </div>
                </Form>
              </div>
              {/* footer card*/}
              <span className="dark:text-[#FE634E] text-[10px] flex justify-center mb-8">
                Copyrights © 2023 Filtration Detergent Laundry. All rights
                reserved
              </span>
            </Card>
          </div>

          {isModalOpen && (
            <Modal
              open={isModalOpen}
              onCancel={() => setIsModalOpen(false)}
              footer={false}
              centered={true}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CloseCircleOutlined
                    style={{ fontSize: "32px", color: "#FF0000" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Oops, login failed...
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        This username not found, please check your username.
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    type={"submit"}
                    onClick={() => setIsModalOpen(false)}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </div>
            </Modal>
          )}

          {modalSuccess && (
            <Modal
              open={modalSuccess}
              onCancel={() => setModalSuccess(false)}
              footer={false}
              centered={true}
              width={600}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CheckCircleOutlined
                    style={{ fontSize: "32px", color: "#A4BE37" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Success
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        Congratulations! Your password has been successfully
                        changed.
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    onClick={() => {
                      navigate("/");
                      window.location.reload();
                    }}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    OK
                  </Button>
                </div>
              </div>
            </Modal>
          )}
        </div>
      </Spin>
    </Transitions>
  );
};

export default NewPassword;
