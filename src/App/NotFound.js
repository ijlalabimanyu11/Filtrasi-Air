import React from "react";
import { Button, Result } from "antd";
import { useNavigate } from "react-router-dom";
import Transitions from "../Components/Transitions";

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <Transitions>
      <div className="flex flex-col items-center">
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
        />
        <Button
          className="flex justify-center bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
          onClick={() => {
            navigate("/");
          }}
        >
          Go Home
        </Button>
      </div>
    </Transitions>
  );
};

export default NotFound;
