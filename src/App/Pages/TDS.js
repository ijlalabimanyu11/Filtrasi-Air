import React, { useState, useRef, useEffect } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import { Card, Button, Table, Input, Modal, Image } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import StatusComponent from "../../Components/StatusComponent";
import moment from "moment";
import { TDS1, TDS2, TDS3 } from "../../Assets/Image";
import { useDispatch, useSelector } from "react-redux";
import { getSliceTDS } from "../../Redux/slices/Filtration";
import Transitions from "../../Components/Transitions";

const TDS = () => {
  const searchInput = useRef(null);
  const dispatch = useDispatch();

  const { data, loading } = useSelector(
    (state) => state.filtration
  );

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [modalOpen1, setModalOpen1] = useState(false);
  const [modalOpen2, setModalOpen2] = useState(false);
  const [modalOpen3, setModalOpen3] = useState(false);

  useEffect(() => {
    dispatch(getSliceTDS());
  }, [dispatch]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const columns = [
    {
      title: "no",
      dataIndex: "ID",
      key: "ID",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "DATE",
      key: "DATE",
      align: "center",
      ...getColumnSearchProps("DATE"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY HH:MM:SS");
      },
    },
    {
      title: "total dissolve solid",
      dataIndex: "TOTAL_DISSOLVE_SOLID",
      key: "TOTAL_DISSOLVE_SOLID",
      align: "center",
      ...getColumnSearchProps("TOTAL_DISSOLVE_SOLID"),
      render: (index) => {
        return <p>{index + " PPM"}</p>;
      },
    },
    {
      title: "status",
      dataIndex: "STATUS",
      key: "STATUS",
      width: "15%",
      ...getColumnSearchProps("STATUS"),
      render: (index) => {
        return <StatusComponent color={index}>{index}</StatusComponent>;
      },
    },
  ];

  console.log(data, "data");

  return (
    <Transitions>
      <LayoutMenu header={"Total Dissolve Solid"}>
        <div className="mx-5 drop-shadow-lg">
          <div className="grid grid-cols-4 gap-x-[30px]">
            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center mb-[10px]">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WHAT IS TOTAL DISSOLVE SOLID
                </p>
              </div>

              <div className="flex justify-center mb-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  TDS stands for Total Dissolved Solids, which refers to the
                  total concentration of all inorganic and organic substances
                  that are dissolved in water. These substances can come from
                  various sources such as natural weathering of rocks and soil,
                  agricultural runoff, industrial discharge, and wastewater
                  treatment plants.
                </p>
              </div>

              <div className="flex justify-center">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen1(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  STANDARD TOTAL DISSOLVE SOLID
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  The acceptable levels of Total Dissolved Solids (TDS) in
                  drinking water may vary depending on the specific country or
                  region. However, the World Health Organization (WHO) has set a
                  guideline value of 500 milligrams per liter (mg/L) for TDS in
                  drinking water.
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen2(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WATER TOTAL DISSOLVE SOLID REGULATION
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Regulating Total Dissolved Solids (TDS) in water is important
                  to ensure its quality for various uses such as drinking,
                  irrigation, and industrial processes. The following are some
                  ways to regulate TDS levels in water:
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen3(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>
          </div>

          <div className="mt-[30px]">
            <Card>
              <div>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  total dissolve solid history
                </p>
              </div>
              <div className="mt-[30px]">
                <Table
                  loading={loading}
                  columns={columns}
                  dataSource={data}
                  scroll={{
                    x: "max-content",
                  }}
                />
              </div>
            </Card>
          </div>

          {/* Modal Card 1 */}
          <Modal
            title="What is Total Dissolve Solid"
            open={modalOpen1}
            onOk={false}
            onCancel={() => setModalOpen1(false)}
            width={630}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen1(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={582}
              height={397}
              src={TDS1}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              TDS stands for Total Dissolved Solids, which refers to the total
              concentration of all inorganic and organic substances that are
              dissolved in water. These substances can come from various sources
              such as natural weathering of rocks and soil, agricultural runoff,
              industrial discharge, and wastewater treatment plants. TDS is
              typically measured in parts per million (ppm) or milligrams per
              liter (mg/L) and is an important indicator of water quality. High
              TDS levels in drinking water can make it taste unpleasant or
              salty, and may also indicate the presence of contaminants such as
              heavy metals or salts, which can be harmful to human health if
              consumed in large amounts over time.
            </p>
          </Modal>

          {/* Modal Card 2 */}
          <Modal
            title="Standard Total Dissolve Solid"
            open={modalOpen2}
            onOk={false}
            onCancel={() => setModalOpen2(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen2(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={578}
              height={556}
              src={TDS2}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Regulating Total Dissolved Solids (TDS) in water is important to
              ensure its quality for various uses such as drinking, irrigation,
              and industrial processes. The following are some ways to regulate
              TDS levels in water: The acceptable levels of Total Dissolved
              Solids (TDS) in drinking water may vary depending on the specific
              country or region. However, the World Health Organization (WHO)
              has set a guideline value of 500 milligrams per liter (mg/L) for
              TDS in drinking water, which is based on the taste threshold and
              not on any potential health effects. In the United States, the
              Environmental Protection Agency (EPA) does not have a specific
              regulation for TDS levels in drinking water, but it does have a
              secondary maximum contaminant level (SMCL) of 500 mg/L, which is a
              non-enforceable guideline for aesthetics and taste.
            </p>
          </Modal>

          {/* Modal Card 3 */}
          <Modal
            title="Total Dissolve Solid Regulation"
            open={modalOpen3}
            onOk={false}
            onCancel={() => setModalOpen3(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen3(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={764}
              height={438}
              src={TDS3}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Regulating Total Dissolved Solids (TDS) in water is important to
              ensure its quality for various uses such as drinking, irrigation,
              and industrial processes. The following are some ways to regulate
              TDS levels in water: <br />
              1. Reverse Osmosis (RO): Reverse osmosis is a water treatment
              method that can remove most of the dissolved solids from water by
              forcing it through a semipermeable membrane. This method is
              commonly used for drinking water and in industries such as
              pharmaceuticals, food and beverage, and electronics.
              <br />
              2. Distillation: Distillation is a process where water is boiled
              and the steam is condensed, leaving behind most of the dissolved
              solids and contaminants. This method is commonly used for
              producing high-purity water for industrial and laboratory
              applications.
              <br />
              3. Ion Exchange: Ion exchange is a water treatment process where
              ions in the water are exchanged with other ions to remove
              dissolved solids and contaminants. This method is commonly used in
              water softening and for removing specific contaminants such as
              nitrates, arsenic, and radionuclides.
              <br />
              4. Activated Carbon Filtration: Activated carbon filtration is a
              process where water is passed through activated carbon, which can
              remove some of the dissolved organic substances. This method is
              commonly used in drinking water treatment and for removing tastes
              and odors.
              <br />
              It's important to note that the method used to regulate TDS levels
              depends on the source and nature of the dissolved solids. For
              example, RO is effective in removing most dissolved solids, while
              ion exchange may be more effective in removing specific
              contaminants. Therefore, it's important to conduct water quality
              tests to determine the most appropriate method for regulating TDS
              levels.
            </p>
          </Modal>
        </div>
      </LayoutMenu>
    </Transitions>
  );
};

export default TDS;
