import React, { useState, useEffect } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import { Card, Spin, Modal, Button } from "antd";
import StatusComponent from "../../Components/StatusComponent";
import { MdGasMeter, MdWaterDrop } from "react-icons/md";
import { SiMoleculer } from "react-icons/si";
import { IoThermometerSharp } from "react-icons/io5";
import Transitions from "../../Components/Transitions";
import { useDispatch, useSelector } from "react-redux";
import { CloseCircleOutlined } from "@ant-design/icons";
import { getSliceRealTIme } from "../../Redux/slices/RealTime";

const RealTime = () => {
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState(false);

  const { data, loading } = useSelector((state) => state.realTime);

  useEffect(() => {
    const interval = setInterval(() => {
      dispatch(getSliceRealTIme());
    }, 10000);

    return () => clearInterval(interval);
  }, []);

  const dataRealTime = data[data.length - 1];

  useEffect(() => {
    if (dataRealTime?.FLAG === 1) {
      setOpenModal(true);
    }
  }, [dataRealTime]);

  return (
    <Transitions>
      <Spin spinning={loading}>
        <LayoutMenu header={"Real Time"}>
          <div className="mx-5 drop-shadow-lg">
            <div className="mb-[30px] inline-flex items-center justify-center flex-row-2 gap-x-3">
              <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                STATUS :
              </p>
              <StatusComponent color={dataRealTime?.STATUS}>
                {dataRealTime?.STATUS ? dataRealTime.STATUS : "-"}
              </StatusComponent>
            </div>

            <div className="real-time grid grid-cols-4 gap-x-[30px]">
              <Card>
                <div className="flex justify-between h-full">
                  <div className="flex-col">
                    <p className="font-semibold text-[18px] text-[#4B465C]/90">
                      {dataRealTime?.TOTAL_DISSOLVE_SOLID
                        ? dataRealTime.TOTAL_DISSOLVE_SOLID
                        : "-"}{" "}
                      PPM
                    </p>
                    <p className="font-normal text-[13px] text-[#4B465C]/80">
                      Total Dissolve Solid
                    </p>
                  </div>
                  <div className="flex justify-center">
                    <div className="my-4 p-1 bg-[#7367F0]/10 rounded-full">
                      <MdGasMeter
                        style={{
                          fontSize: "32px",
                          color: "#7367F0",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </Card>
              <Card>
                <div className="flex justify-between h-full">
                  <div className="flex-col">
                    <p className="font-semibold text-[18px] text-[#4B465C]/90">
                      {dataRealTime?.ACIDITY ? dataRealTime.ACIDITY : "-"} pH
                    </p>
                    <p className="font-normal text-[13px] text-[#4B465C]/80">
                      Acidity
                    </p>
                  </div>
                  <div className="flex justify-center">
                    <div className="my-4 p-1 bg-[#28C76F]/10 rounded-full">
                      <MdWaterDrop
                        style={{
                          fontSize: "32px",
                          color: "#28C76F",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </Card>
              <Card>
                <div className="flex justify-between h-full">
                  <div className="flex-col">
                    <p className="font-semibold text-[18px] text-[#4B465C]/90">
                      {dataRealTime?.TURBIDITY ? dataRealTime.TURBIDITY : "-"}{" "}
                      NTU
                    </p>
                    <p className="font-normal text-[13px] text-[#4B465C]/80">
                      Turbidity
                    </p>
                  </div>
                  <div className="flex justify-center">
                    <div className="my-4 p-1 bg-[#EA5455]/10 rounded-full">
                      <SiMoleculer
                        style={{
                          fontSize: "32px",
                          color: "#EA5455",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </Card>
              <Card>
                <div className="flex justify-between h-full">
                  <div className="flex-col">
                    <p className="font-semibold text-[18px] text-[#4B465C]/90">
                      {dataRealTime?.TEMPERATURE
                        ? dataRealTime.TEMPERATURE
                        : "-"}{" "}
                      °C
                    </p>
                    <p className="font-normal text-[13px] text-[#4B465C]/80">
                      Temperature
                    </p>
                  </div>
                  <div className="flex justify-center">
                    <div className="my-4 p-1 bg-[#FF9F43]/10 rounded-full">
                      <IoThermometerSharp
                        style={{
                          fontSize: "32px",
                          color: "#FF9F43",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </Card>
            </div>

            <div className="mt-[30px]">
              <Card className="flex flex-col">
                <div>
                  <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                    information
                  </p>
                </div>
                <div className="mt-[30px] inline-flex items-center flex-row-2 gap-x-3">
                  <StatusComponent color={"VERY GOOD"}>
                    VERY GOOD
                  </StatusComponent>
                  <p className="font-semibold text-[14px] text-[#000000]">
                    {`TDS <200 PPM, Turbidity 5-25 NTU, Acidity 7 pH, Temperature 18-25 °C`}
                  </p>
                </div>
                <div></div>
                <div className="mt-[30px] inline-flex items-center flex-row-2 gap-x-3">
                  <StatusComponent color={"GOOD"}>
                    <p className="px-5 text-white font-semibold">GOOD</p>
                  </StatusComponent>
                  <p className="font-semibold text-[14px] text-[#000000]">
                    {`TDS 201-600 PPM, Turbidity 26-70 NTU, Acidity 6-6.9 pH or 7.1-8 pH, Temperature 26-29 °C`}
                  </p>
                </div>
                <div></div>
                <div className="mt-[30px] inline-flex items-center flex-row-2 gap-x-3">
                  <StatusComponent color={"BAD"}>
                    <p className="px-[26px] text-white font-semibold">BAD</p>
                  </StatusComponent>
                  <p className="font-semibold text-[14px] text-[#000000]">
                    {`TDS 601-1400 PPM, Turbidity 71-100 NTU, Acidity 5-5.9 pH or 8.1-9 pH, Temperature 30-35 °C`}
                  </p>
                </div>
                <div></div>
                <div className="mt-[30px] inline-flex items-center flex-row-2 gap-x-3">
                  <StatusComponent color={"VERY BAD"}>
                    <p className="px-2 text-white font-semibold">VERY BAD</p>
                  </StatusComponent>
                  <p className="font-semibold text-[14px] text-[#000000] ">
                    {`TDS 1401-1500 PPM, Turbidity 101-500 NTU, Acidity 4-4.9 pH or 10-10.9 pH, Temperature 36-40 °C`}
                  </p>
                </div>
                <div></div>
                <div className="mt-[30px] inline-flex items-center flex-row-2 gap-x-3">
                  <StatusComponent color={"WARNING"}>
                    <p className="px-[7px] text-white font-semibold">WARNING</p>
                  </StatusComponent>
                  <p className="font-semibold text-[14px] text-[#000000] uppercase">
                    {`TDS >1500 PPM, Turbidity >500 NTU, Acidity <4 pH or > 11 pH, Temperature >40 °C`}
                  </p>
                </div>
              </Card>
            </div>
          </div>

          {openModal && (
            <Modal
              open={openModal}
              onCancel={() => setOpenModal(false)}
              footer={false}
              centered={true}
            >
              <div className={"flex flex-col w-full"}>
                <div className={"flex px-8 py-8"}>
                  <CloseCircleOutlined
                    style={{ fontSize: "32px", color: "#FF0000" }}
                    className="my-2"
                  />
                  <div className="w-full flex-col">
                    <div className="pl-4">
                      <span className="text-xl font-bold  text-[#000000]">
                        Warning!
                      </span>
                    </div>
                    <div className="pl-4 pt-4">
                      <span className={"text-l"}>
                        Your media filtration must be changed to new!
                      </span>
                    </div>
                  </div>
                </div>

                <div className={"w-full justify-end flex py-5 px-5"}>
                  <Button
                    type={"submit"}
                    onClick={() => {
                      setOpenModal(false);
                    }}
                    className="w-1/4 flex justify-center items-center rounded-md bg-[#FE634E] py-5 px-5 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </div>
            </Modal>
          )}
        </LayoutMenu>
      </Spin>
    </Transitions>
  );
};

export default RealTime;
