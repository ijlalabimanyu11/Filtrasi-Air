import React, { useState, useEffect, useRef } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import { Card, Input, Table, Button, Form, DatePicker, Spin } from "antd";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
} from "chart.js";
import { Chart } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import {
  SearchOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import moment from "moment";
import StatusComponent from "../../Components/StatusComponent";
import { useDispatch, useSelector } from "react-redux";
import {
  getSliceDataMonthly,
  getSliceDataResult,
  getSliceDataWeekly,
} from "../../Redux/slices/Filtration";
import dayjs from "dayjs";
import Transitions from "../../Components/Transitions";

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  ChartDataLabels
);
const { RangePicker } = DatePicker;

const Dashboard = () => {
  const searchInput = useRef(null);
  const dispatch = useDispatch();
  const yearDate = new Date().getFullYear();
  const week1 = dayjs().day(1).format("YYYY-MM-DD");
  const week2 = dayjs().day(7).format("YYYY-MM-DD");

  const initialBody = {
    yearMonthly: null,
  };

  const { dataMonthly, dataWeekly, data, loading } = useSelector(
    (state) => state.filtration
  );

  const [{ yearMonthly }, setState] = useState(initialBody);

  const [slideLeft, setSlideLeft] = useState(0);
  const [hideButtonLeft, setHideButtonLeft] = useState(true);
  const [hideButtonRight, setHideButtonRight] = useState(false);
  const [sliderWidth, setSliderWidth] = useState(0);

  const [slideLeftWeekly, setSlideLeftWeekly] = useState(0);
  const [hideButtonLeftWeekly, setHideButtonLeftWeekly] = useState(true);
  const [hideButtonRightWeekly, setHideButtonRightWeekly] = useState(false);
  const [sliderWidthWeekly, setSliderWidthWeekly] = useState(0);

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [dates, setDates] = useState(null);
  const [value, setValue] = useState(null);
  const [booleanMonthly, setBooleanMonthly] = useState(false);
  const [booleanWeekly, setBooleanWeekly] = useState(false);

  const labelsWeekly = dataWeekly?.map((a) => a["DAYNAME(DATE)"]);

  const labelsWeek = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  useEffect(() => {
    dispatch(getSliceDataResult());
  }, []);

  useEffect(() => {
    if (booleanMonthly === false) {
      dispatch(getSliceDataMonthly({ year: yearDate }));
    } else {
      dispatch(getSliceDataMonthly({ year: dayjs(yearMonthly) }));
    }
  }, [dispatch, booleanMonthly, yearMonthly, yearDate]);

  useEffect(() => {
    if (booleanWeekly === false) {
      // dispatch(
      //   getSliceDataWeekly({
      //     startDate: week1,
      //     endDate: week2,
      //   })
      // );
    } else {
      dispatch(
        getSliceDataWeekly({
          startDate: dayjs(value[0], "YYYY-MM-DD"),
          endDate: dayjs(value[1], "YYYY-MM-DD"),
        })
      );
    }
  }, [dispatch, booleanWeekly, value, week1, week2]);

  useEffect(() => {
    setSliderWidth(
      document.getElementById("hscroll").scrollWidth -
        document.getElementById("hscroll").offsetWidth
    );
  }, []);

  useEffect(() => {
    setSliderWidthWeekly(
      document.getElementById("Weeklyscroll").scrollWidth -
        document.getElementById("Weeklyscroll").offsetWidth
    );
  }, []);

  //on arrow click
  const moveRight = () => {
    const el = document.getElementById(`hscroll`);
    setSlideLeft((el.scrollLeft += 200));
  };

  const moveLeft = () => {
    const el = document.getElementById(`hscroll`);
    setSlideLeft((el.scrollLeft -= 200));
  };

  const moveRightWeekly = () => {
    const el = document.getElementById(`Weeklyscroll`);
    setSlideLeftWeekly((el.scrollLeft += 200));
  };

  const moveLeftWeekly = () => {
    const el = document.getElementById(`Weeklyscroll`);
    setSlideLeftWeekly((el.scrollLeft -= 200));
  };

  const onHScroll = () => {
    const el = document.getElementById(`hscroll`).scrollLeft;
    if (el > 0) {
      setHideButtonLeft(false);
    } else {
      setHideButtonLeft(true);
    }
    if (el < sliderWidth) {
      setHideButtonRight(false);
    } else {
      setHideButtonRight(true);
    }
  };

  const onHScrollWeekly = () => {
    const el = document.getElementById(`Weeklyscroll`).scrollLeft;
    if (el > 0) {
      setHideButtonLeftWeekly(false);
    } else {
      setHideButtonLeftWeekly(true);
    }
    if (el < sliderWidthWeekly) {
      setHideButtonRightWeekly(false);
    } else {
      setHideButtonRightWeekly(true);
    }
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const columns = [
    {
      title: "no",
      dataIndex: "ID",
      key: "ID",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "DATE",
      key: "DATE",
      align: "center",
      ...getColumnSearchProps("date"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY");
      },
    },
    {
      title: "total dissolve solid",
      dataIndex: "TOTAL_DISSOLVE_SOLID",
      key: "TOTAL_DISSOLVE_SOLID",
      align: "center",
      ...getColumnSearchProps("TOTAL_DISSOLVE_SOLID"),
      render: (index) => {
        return <p>{index + " PPM"}</p>;
      },
    },
    {
      title: "acidity",
      dataIndex: "ACIDITY",
      key: "ACIDITY",
      align: "center",
      ...getColumnSearchProps("ACIDITY"),
      render: (index) => {
        return <p>{index + " pH"}</p>;
      },
    },
    {
      title: "turbidity",
      dataIndex: "TURBIDITY",
      key: "TURBIDITY",
      align: "center",
      ...getColumnSearchProps("TURBIDITY"),
      render: (index) => {
        return <p>{index + " NTU"}</p>;
      },
    },
    {
      title: "temperature",
      dataIndex: "TEMPERATURE",
      key: "TEMPERATURE",
      align: "center",
      ...getColumnSearchProps("TEMPERATURE"),
      render: (index) => {
        return <p>{index} &deg;C</p>;
      },
    },
    {
      title: "status",
      dataIndex: "STATUS",
      key: "STATUS",
      ...getColumnSearchProps("STATUS"),
      render: (index) => {
        return <StatusComponent color={index}>{index}</StatusComponent>;
      },
    },
  ];

  // Data Bar
  const dataTDS = {
    labels,
    datasets: [
      {
        data: dataMonthly
          ?.filter((a) => a.TDS_MONTHLY)
          .map((b) => Number(b.TDS_MONTHLY).toFixed(2)),
        backgroundColor: "rgb(115,103,240)",
        borderRadius: 4,
        borderSkipped: false,
      },
    ],
  };

  const dataAcidity = {
    labels,
    datasets: [
      {
        data: dataMonthly
          ?.filter((a) => a.ACIDITY_MONTHLY)
          .map((b) => Number(b.ACIDITY_MONTHLY).toFixed(2)),
        backgroundColor: "rgb(40, 199, 111)",
        borderRadius: 4,
        borderSkipped: false,
      },
    ],
  };

  const dataTurbidity = {
    labels,
    datasets: [
      {
        data: dataMonthly
          ?.filter((a) => a.TURBIDITY_MONTHLY)
          .map((b) => Number(b.TURBIDITY_MONTHLY).toFixed(2)),
        backgroundColor: "rgb(234, 84, 85)",
        borderRadius: 4,
        borderSkipped: false,
      },
    ],
  };

  const dataTemperature = {
    labels,
    datasets: [
      {
        data: dataMonthly
          ?.filter((a) => a.TEMPERATURE_MONTHLY)
          .map((b) => Number(b.TEMPERATURE_MONTHLY).toFixed(2)),
        backgroundColor: "rgb(255, 159, 67)",
        borderRadius: 4,
        borderSkipped: false,
      },
    ],
  };

  // Styling Bar
  const options = {
    plugins: {
      responsive: true,
      datalabels: {
        display: true,
        color: "black",
        anchor: "end",
        offset: -20,
        align: "start",
      },
      legend: {
        display: false,
      },
      padding: {
        bottom: 10,
      },
    },
    aspectRatio: 5 / 3,
    layout: {
      padding: {
        top: 32,
        right: 16,
        bottom: 16,
        left: 8,
      },
      elements: {
        line: {
          fill: false,
          tension: 0.4,
        },
        point: {
          hoverRadius: 7,
          radius: 5,
        },
      },
      scales: {
        y: {
          stacked: true,
        },
        x: {
          stacked: true,
        },
      },
    },
  };

  // Weekly History
  const dataWeeklyTDS = {
    labels: labelsWeekly && labelsWeekly.length > 0 ? labelsWeekly : labelsWeek,
    datasets: [
      {
        label: "Total Dissolve Solid",
        data: dataWeekly
          ?.filter((a) => a.TOTAL_DISSOLVE_SOLID)
          .map((b) => (b.TOTAL_DISSOLVE_SOLID)),
        borderColor: "rgb(115,103,240)",
        backgroundColor: "rgb(115,103,240)",
      },
    ],
  };

  const dataWeeklyAcidity = {
    labels: labelsWeekly && labelsWeekly.length > 0 ? labelsWeekly : labelsWeek,
    datasets: [
      {
        label: "Acidity",
        data: dataWeekly
          ?.filter((a) => a.ACIDITY)
          .map((b) => (b.ACIDITY)),
        borderColor: "rgb(40, 199, 111)",
        backgroundColor: "rgb(40, 199, 111)",
      },
    ],
  };

  const dataWeeklyTurbidity = {
    labels: labelsWeekly && labelsWeekly.length > 0 ? labelsWeekly : labelsWeek,
    datasets: [
      {
        label: "Turbidity",
        data: dataWeekly
          ?.filter((a) => a.TURBIDITY)
          .map((b) => (b.TURBIDITY)),
        borderColor: "rgb(234, 84, 85)",
        backgroundColor: "rgb(234, 84, 85)",
      },
    ],
  };

  const dataWeeklyTemperature = {
    labels: labelsWeekly && labelsWeekly.length > 0 ? labelsWeekly : labelsWeek,
    datasets: [
      {
        label: "Temperature",
        data: dataWeekly
          ?.filter((a) => a.TEMPERATURE)
          .map((b) => (b.TEMPERATURE)),
        borderColor: "rgb(255, 159, 67)",
        backgroundColor: "rgb(255, 159, 67)",
      },
    ],
  };

  // Styling Line
  const optionsLine = {
    plugins: {
      responsive: true,
      datalabels: {
        display: true,
        color: "black",
        anchor: "end",
        offset: -20,
        align: "start",
      },
      legend: {
        display: false,
      },
      padding: {
        bottom: 10,
      },
    },
    aspectRatio: 5 / 3,
    layout: {
      padding: {
        top: 32,
        right: 16,
        bottom: 16,
        left: 8,
      },
      elements: {
        line: {
          fill: false,
          tension: 0.4,
        },
        point: {
          hoverRadius: 7,
          radius: 5,
        },
      },
      scales: {
        y: {
          stacked: true,
        },
        x: {
          stacked: true,
        },
      },
    },
  };

  const onFinishWeekly = async () => {
    let data;
    data = {
      startDate: dayjs(value[0], "YYYY-MM-DD"),
      endDate: dayjs(value[1], "YYYY-MM-DD"),
    };
    console.log(data);
    dispatch(getSliceDataWeekly(data));
  };

  const onFinishMonthly = (formValue) => {
    let data;
    data = {
      year: moment(formValue.year).format("YYYY"),
    };
    dispatch(getSliceDataMonthly(data));
  };

  const disabledDate = (current) => {
    if (!dates) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], "days") >= 7;
    const tooEarly = dates[1] && dates[1].diff(current, "days") >= 7;
    return !!tooEarly || !!tooLate;
  };

  const onOpenChange = (open) => {
    if (open) {
      setDates([null, null]);
    } else {
      setDates(null);
    }
  };

  return (
    <Transitions>
      <LayoutMenu header={"Dashboard"}>
        <div className="mx-5 drop-shadow-lg">
          <div>
            <Card>
              <Spin spinning={loading}>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  Weekly Overview History
                </p>
                <Form
                  name="basic"
                  layout="inline"
                  onFinish={onFinishWeekly}
                  className="mt-[30px] flex flex-row"
                >
                  <Form.Item
                    name={"date"}
                    label={"Date"}
                    rules={[
                      {
                        required: true,
                        message: "Please input your Date!",
                      },
                    ]}
                  >
                    <RangePicker
                      value={dates || value}
                      disabledDate={disabledDate}
                      onCalendarChange={(val) => {
                        setDates(val);
                      }}
                      onChange={(val) => {
                        setValue(val);
                        setBooleanWeekly(true);
                      }}
                      onOpenChange={onOpenChange}
                      changeOnBlur
                      allowClear={false}
                      // defaultValue={[dayjs().day(1), dayjs().day(7)]}
                    />
                  </Form.Item>
                  {/* <Form.Item>
                  <Button
                    htmlType="submit"
                    className="flex justify-center items-center rounded-md bg-[#FE634E] py-4 px-4 text-base font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Search
                  </Button>
                </Form.Item> */}
                </Form>

                <div className="w-full h-full gap-x-[30px] flex flex-row justify-center">
                  <div className="items-center inline-flex">
                    <Button
                      onClick={moveLeftWeekly}
                      disabled={hideButtonLeftWeekly}
                      className="flex justify-center rounded-md bg-[#FE634E] px-3 py-2 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                    >
                      <ArrowLeftOutlined
                        style={{
                          cursor: "pointer",
                          color: "#FFFFFF",
                        }}
                      />
                    </Button>
                  </div>

                  <div
                    className="mt-[30px] flex-container real-time grid grid-cols-4 gap-x-[30px]"
                    onScroll={() => onHScrollWeekly()}
                    id={`Weeklyscroll`}
                  >
                    <Card>
                      <h1 className="font-semibold text-base tracking-wide">
                        Total Dissolve Solid
                      </h1>

                      <Chart
                        type="line"
                        data={dataWeeklyTDS}
                        options={optionsLine}
                        style={{
                          width: 500,
                        }}
                      />
                    </Card>

                    <Card>
                      <h1 className="font-semibold text-base tracking-wide">
                        Acidity
                      </h1>

                      <Chart
                        type="line"
                        data={dataWeeklyAcidity}
                        options={optionsLine}
                        style={{
                          width: 500,
                        }}
                      />
                    </Card>

                    <Card>
                      <h1 className="font-semibold text-base tracking-wide">
                        Turbidity
                      </h1>

                      <Chart
                        type="line"
                        data={dataWeeklyTurbidity}
                        options={options}
                        style={{
                          width: 500,
                        }}
                      />
                    </Card>

                    <Card>
                      <h1 className="font-semibold text-base tracking-wide">
                        Temperature
                      </h1>

                      <Chart
                        type="line"
                        data={dataWeeklyTemperature}
                        options={optionsLine}
                        style={{
                          width: 500,
                        }}
                      />
                    </Card>
                  </div>
                  <div className="flex items-center">
                    <Button
                      onClick={moveRightWeekly}
                      disabled={hideButtonRightWeekly}
                      className="flex justify-center rounded-md bg-[#FE634E] px-3 py-2 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                    >
                      <ArrowRightOutlined
                        style={{
                          color: "#FFFFFF",
                          cursor: "pointer",
                        }}
                      />
                    </Button>
                  </div>
                </div>
              </Spin>
            </Card>
          </div>

          <Card className="mt-[30px]">
            <Spin spinning={loading}>
              <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                Monthly Overview History
              </p>
              <Form
                layout="inline"
                onFinish={onFinishMonthly}
                className="mt-[30px] flex flex-row"
              >
                <Form.Item
                  name={"year"}
                  label={"Year"}
                  rules={[
                    {
                      required: true,
                      message: "Please input your Year!",
                    },
                  ]}
                >
                  <DatePicker
                    style={{ width: "100%" }}
                    picker="year"
                    onChange={(date) =>
                      setState(
                        (prevState) => ({
                          ...prevState,
                          yearMonthly: date,
                        }),
                        setBooleanMonthly(true)
                      )
                    }
                    value={yearMonthly}
                    allowClear={false}
                    defaultValue={dayjs()}
                  />
                </Form.Item>
              </Form>

              <div className="w-full h-full gap-x-[30px] flex flex-row justify-center">
                <div className="justify-center items-center flex">
                  <Button
                    onClick={moveLeft}
                    disabled={hideButtonLeft}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-2 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    <ArrowLeftOutlined
                      style={{
                        cursor: "pointer",
                        color: "#FFFFFF",
                      }}
                    />
                  </Button>
                </div>

                <div
                  className="mt-[30px] flex-container real-time grid grid-cols-4 gap-x-[30px]"
                  onScroll={() => onHScroll()}
                  id={`hscroll`}
                >
                  <Card>
                    <h1 className="font-semibold text-base tracking-wide">
                      Total Dissolve Solid
                    </h1>

                    <Chart
                      type="bar"
                      data={dataTDS}
                      options={options}
                      style={{
                        width: 500,
                      }}
                    />
                  </Card>

                  <Card>
                    <h1 className="font-semibold text-base tracking-wide">
                      Acidity
                    </h1>

                    <Chart
                      type="bar"
                      data={dataAcidity}
                      options={options}
                      style={{
                        width: 500,
                      }}
                    />
                  </Card>

                  <Card>
                    <h1 className="font-semibold text-base tracking-wide">
                      Turbidity
                    </h1>

                    <Chart
                      type="bar"
                      data={dataTurbidity}
                      options={options}
                      style={{
                        width: 500,
                      }}
                    />
                  </Card>

                  <Card>
                    <h1 className="font-semibold text-base tracking-wide">
                      Temperature
                    </h1>

                    <Chart
                      type="bar"
                      data={dataTemperature}
                      options={options}
                      style={{
                        width: 500,
                      }}
                    />
                  </Card>
                </div>
                <div className="flex items-center">
                  <Button
                    onClick={moveRight}
                    disabled={hideButtonRight}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-2 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    <ArrowRightOutlined
                      style={{
                        color: "#FFFFFF",
                        cursor: "pointer",
                      }}
                    />
                  </Button>
                </div>
              </div>
            </Spin>
          </Card>

          <div className="mt-[30px]">
            <Card>
              <div>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  data result history
                </p>
              </div>
              <div className="mt-[30px]">
                <Table
                  loading={loading}
                  columns={columns}
                  dataSource={data}
                  scroll={{
                    x: "max-content",
                  }}
                />
              </div>
            </Card>
          </div>
        </div>
      </LayoutMenu>
    </Transitions>
  );
};

export default Dashboard;
