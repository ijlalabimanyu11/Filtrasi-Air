import React, { useState, useEffect, useRef } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import { Card, Button, Table, Input, Modal, Image } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import StatusComponent from "../../Components/StatusComponent";
import moment from "moment";
import { Temperature1, Temperature2, Temperature3 } from "../../Assets/Image";
import { useDispatch, useSelector } from "react-redux";
import { getSliceTemperature } from "../../Redux/slices/Filtration";
import Transitions from "../../Components/Transitions";

const Temperature = () => {
  const searchInput = useRef(null);
  const dispatch = useDispatch();

  const { data, loading } = useSelector(
    (state) => state.filtration
  );

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [modalOpen1, setModalOpen1] = useState(false);
  const [modalOpen2, setModalOpen2] = useState(false);
  const [modalOpen3, setModalOpen3] = useState(false);

  useEffect(() => {
    dispatch(getSliceTemperature());
  }, [dispatch]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const columns = [
    {
      title: "no",
      dataIndex: "ID",
      key: "ID",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "DATE",
      key: "DATE",
      align: "center",
      ...getColumnSearchProps("DATE"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY HH:MM:SS");
      },
    },
    {
      title: "temperature",
      dataIndex: "TEMPERATURE",
      key: "TEMPERATURE",
      align: "center",
      ...getColumnSearchProps("TEMPERATURE"),
      render: (index) => {
        return <p>{index} &deg;C</p>;
      },
    },
    {
      title: "status",
      dataIndex: "STATUS",
      key: "STATUS",
      width: "15%",
      ...getColumnSearchProps("STATUS"),
      render: (index) => {
        return <StatusComponent color={index}>{index}</StatusComponent>;
      },
    },
  ];

  return (
    <Transitions>
      <LayoutMenu header={"Temperature"}>
        <div className="mx-5 drop-shadow-lg">
          <div className="grid grid-cols-3 gap-x-[30px]">
            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WHAT IS TEMPERATURE
                </p>
              </div>
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Temperature is a measure of the degree of hotness or coldness
                  of a substance or object. It is a fundamental physical
                  property and is commonly measured in units of degrees Celsius
                  (°C), Fahrenheit (°F), or Kelvin (K).
                </p>
              </div>
              <div className="flex justify-center h-full">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen1(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold  text-center text-[14px] text-[#FE634E]">
                  STANDARD TEMPERATURE
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  The standard temperature of the water is typically defined as
                  25 degrees Celsius (77 degrees Fahrenheit) at standard
                  atmospheric pressure (1 atmosphere or 101.3 kilopascals).
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen2(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-[14px] text-center text-[#FE634E]">
                  WATER TEMPERATURE REGULATION
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Water temperature regulation refers to the mechanisms and
                  processes that maintain the temperature of water within a
                  certain range, despite changes in external conditions such as
                  sunlight, air temperature, and weather patterns.
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen3(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>
          </div>

          <div className="mt-[30px]">
            <Card>
              <div>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  temperature history
                </p>
              </div>
              <div className="mt-[30px]">
                <Table
                  loading={loading}
                  columns={columns}
                  dataSource={data}
                  scroll={{
                    x: "max-content",
                  }}
                />
              </div>
            </Card>
          </div>

          {/* Modal Card 1 */}
          <Modal
            title="What is Temperature"
            open={modalOpen1}
            onOk={false}
            onCancel={() => setModalOpen1(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen1(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={274}
              src={Temperature1}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Temperature is a measure of the degree of hotness or coldness of a
              substance or object. It is a fundamental physical property and is
              commonly measured in units of degrees Celsius (°C), Fahrenheit
              (°F), or Kelvin (K). The temperature of a substance is related to
              the average kinetic energy of its molecules, with higher
              temperatures indicating greater kinetic energy and faster
              molecular motion.
            </p>
          </Modal>

          {/* Modal Card 2 */}
          <Modal
            title="Standard Temperature"
            open={modalOpen2}
            onOk={false}
            onCancel={() => setModalOpen2(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen2(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={500}
              src={Temperature2}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              The standard temperature of the water is typically defined as 25
              degrees Celsius (77 degrees Fahrenheit) at standard atmospheric
              pressure (1 atmosphere or 101.3 kilopascals). This standard
              temperature is often used as a reference point for many scientific
              measurements and calculations involving water, such as determining
              the solubility of substances in water, or the specific heat
              capacity of water. Overall, the standard temperature of water
              serves as a useful reference point for many scientific purposes,
              but it's important to keep in mind that water temperatures can
              vary significantly in different environments and under different
              conditions.
            </p>
          </Modal>

          {/* Modal Card 3 */}
          <Modal
            title="Water Temperature Regulation"
            open={modalOpen3}
            onOk={false}
            onCancel={() => setModalOpen3(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen3(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={500}
              src={Temperature3}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Water temperature regulation is important in various applications
              such as drinking water, industrial processes, and aquatic
              ecosystems. Here are some ways to regulate water temperature:{" "}
              <br />
              1. Cooling Towers: Cooling towers are commonly used in industrial
              processes to remove heat from water by evaporation. Water is
              circulated through the cooling tower, and the heat is transferred
              to the air through evaporation. This method is effective in
              reducing water temperature in large volumes. <br />
              2. Heat Exchangers: Heat exchangers are devices that transfer heat
              from one fluid to another without mixing them. They are commonly
              used in industrial processes to regulate water temperature by
              exchanging heat between the water and a cooling fluid such as air,
              refrigerant, or another water stream. <br />
              3. Insulation: Insulating water pipes and storage tanks can reduce
              heat loss and maintain the desired water temperature. This method
              is commonly used in hot water distribution systems and for storing
              hot water for later use. <br />
              4. Shade: Providing shade over water bodies can reduce the amount
              of sunlight absorbed, which can help maintain cooler water
              temperatures. This method is commonly used in aquatic ecosystems
              to protect fish and other aquatic organisms from the effects of
              high water temperature. <br />
              5. Mixing: Mixing water can help regulate temperature by promoting
              heat exchange between different water layers. This method is
              commonly used in water storage tanks and reservoirs to prevent
              thermal stratification and maintain a consistent temperature.{" "}
              <br />
              Overall, it's important to regulate water temperature to ensure
              its quality for various applications and to protect aquatic
              ecosystems. The method used to regulate water temperature depends
              on the specific application and the nature of the water source.
            </p>
          </Modal>
        </div>
      </LayoutMenu>
    </Transitions>
  );
};

export default Temperature;
