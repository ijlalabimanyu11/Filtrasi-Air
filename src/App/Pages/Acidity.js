import React, { useState, useEffect, useRef } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import { Card, Button, Table, Input, Modal, Image } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import StatusComponent from "../../Components/StatusComponent";
import moment from "moment";
import { Acidity1, Acidity2, Acidity3 } from "../../Assets/Image";
import { useDispatch, useSelector } from "react-redux";
import { getSliceAcidity } from "../../Redux/slices/Filtration";
import Transitions from "../../Components/Transitions";

const Acidity = () => {
  const searchInput = useRef(null);
  const dispatch = useDispatch();

  const { data, loading } = useSelector((state) => state.filtration);

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [modalOpen1, setModalOpen1] = useState(false);
  const [modalOpen2, setModalOpen2] = useState(false);
  const [modalOpen3, setModalOpen3] = useState(false);

  useEffect(() => {
    dispatch(getSliceAcidity());
  }, [dispatch]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const columns = [
    {
      title: "no",
      dataIndex: "ID",
      key: "ID",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "DATE",
      key: "DATE",
      align: "center",
      ...getColumnSearchProps("DATE"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY HH:MM:SS");
      },
    },
    {
      title: "acidity",
      dataIndex: "ACIDITY",
      key: "ACIDITY",
      align: "center",
      ...getColumnSearchProps("ACIDITY"),
      render: (index) => {
        return <p>{index + " pH"}</p>;
      },
    },
    {
      title: "status",
      dataIndex: "STATUS",
      key: "STATUS",
      width: "15%",
      ...getColumnSearchProps("STATUS"),
      render: (index) => {
        return <StatusComponent color={index}>{index}</StatusComponent>;
      },
    },
  ];

  return (
    <Transitions>
      <LayoutMenu header={"Acidity"}>
        <div className="mx-5 drop-shadow-lg">
          <div className="grid grid-cols-3 gap-x-[30px]">
            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WHAT IS ACIDITY
                </p>
              </div>
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Acidity refers to the level of acid present in a substance,
                  such as a liquid or a food. In chemistry, acidity is typically
                  measured using the pH scale, which ranges from 0 to 14.
                </p>
              </div>
              <div className="flex justify-center h-full">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen1(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  STANDARD ACIDITY
                </p>
              </div>
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  The standard acidity in chemistry is often referred to as the
                  acidity constant or dissociation constant (Ka) of a substance.
                  This constant is a measure of the degree to which an acid will
                  dissociate (or split apart).
                </p>
              </div>
              <div className="flex justify-center h-full">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen2(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WATER ACIDITY REGULATION
                </p>
              </div>
              <div className="flex justify-center h-full mb-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Water acidity regulation refers to the process of controlling
                  and maintaining the pH level of water to ensure that it is
                  safe for human consumption and does not cause harm to the
                  environment.
                </p>
              </div>
              <div className="flex justify-center h-full">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen3(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>
          </div>

          <div className="mt-[30px]">
            <Card>
              <div>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  acidity history
                </p>
              </div>
              <div className="mt-[30px]">
                <Table
                  loading={loading}
                  columns={columns}
                  dataSource={data}
                  scroll={{
                    x: "max-content",
                  }}
                />
              </div>
            </Card>
          </div>

          {/* Modal Card 1 */}
          <Modal
            title="What is Acidity"
            open={modalOpen1}
            onOk={false}
            onCancel={() => setModalOpen1(false)}
            width={650}
            centered
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen1(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
          >
            <Image
              preview={false}
              width={620}
              height={349}
              src={Acidity1}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Acidity refers to the level of acid present in a substance, such
              as a liquid or a food. In chemistry, acidity is typically measured
              using the pH scale, which ranges from 0 to 14. A pH value of 7 is
              considered neutral, while a pH value below 7 is acidic and a pH
              value above 7 is basic (or alkaline).
            </p>
          </Modal>

          {/* Modal Card 2 */}
          <Modal
            title="Standard Acidity"
            open={modalOpen2}
            onOk={false}
            onCancel={() => setModalOpen2(false)}
            width={650}
            centered
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen2(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
          >
            <Image
              preview={false}
              width={620}
              height={349}
              src={Acidity2}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              The standard acidity in chemistry is often referred to as the
              acidity constant or dissociation constant (Ka) of a substance.
              This constant is a measure of the degree to which an acid will
              dissociate (or split apart) into its constituent ions when it is
              dissolved in water. <br />
              In addition to Ka, pH is another common measure of acidity used in
              chemistry. pH is a measure of the concentration of hydrogen ions
              (H+) in a solution, with lower pH values indicating higher
              acidity. The pH scale ranges from 0 to 14, with 7 being neutral,
              values below 7 being acidic, and values above 7 being basic (or
              alkaline).
            </p>
          </Modal>

          {/* Modal Card 3 */}
          <Modal
            title="Water Acidity Regulation"
            open={modalOpen3}
            onOk={false}
            onCancel={() => setModalOpen3(false)}
            width={650}
            centered
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen3(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
          >
            <Image
              preview={false}
              width={604}
              height={523}
              src={Acidity3}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Water acidity regulation refers to the process of controlling and
              maintaining the pH level of water to ensure that it is safe for
              human consumption and does not cause harm to the environment. The
              pH level of water can be influenced by various factors, including
              natural processes such as the weathering of rocks and soil, as
              well as human activities such as industrial processes and
              agriculture. In order to regulate water acidity, a number of
              methods can be employed, such as: <br />
              1. Adding chemicals: Chemicals such as lime or sodium hydroxide
              can be added to water to increase its pH level and make it less
              acidic. <br />
              2. Filtration: Filtration systems can be used to remove
              contaminants from water that may contribute to its acidity, such
              as heavy metals and other pollutants. <br />
              3. Monitoring: Regular monitoring of water quality is essential
              for identifying changes in acidity levels and implementing
              appropriate measures to regulate it. <br />
              4. Environmental management: Strategies such as reducing
              industrial and agricultural pollution, protecting wetlands and
              other natural habitats, and managing stormwater runoff can also
              help to regulate water acidity levels. <br />
              Overall, water acidity regulation is an important process for
              ensuring the safety and quality of water for human consumption and
              environmental health.
            </p>
          </Modal>
        </div>
      </LayoutMenu>
    </Transitions>
  );
};

export default Acidity;
