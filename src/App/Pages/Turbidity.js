import { Card, Button, Table, Input, Modal, Image } from "antd";
import React, { useState, useEffect, useRef } from "react";
import LayoutMenu from "../../Components/LayoutMenu";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import StatusComponent from "../../Components/StatusComponent";
import moment from "moment";
import { Turbidity1, Turbidity2, Turbidity3 } from "../../Assets/Image/index";
import { useDispatch, useSelector } from "react-redux";
import { getSliceTurbidity } from "../../Redux/slices/Filtration";
import Transitions from "../../Components/Transitions";

const Turbidity = () => {
  const searchInput = useRef(null);
  const dispatch = useDispatch();

  const { data, loading } = useSelector(
    (state) => state.filtration
  );

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [modalOpen1, setModalOpen1] = useState(false);
  const [modalOpen2, setModalOpen2] = useState(false);
  const [modalOpen3, setModalOpen3] = useState(false);

  useEffect(() => {
    dispatch(getSliceTurbidity());
  }, [dispatch]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "no",
      dataIndex: "ID",
      key: "ID",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "DATE",
      key: "DATE",
      align: "center",
      ...getColumnSearchProps("DATE"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY HH:MM:SS");
      },
    },
    {
      title: "turbidity",
      dataIndex: "TURBIDITY",
      key: "TURBIDITY",
      align: "center",
      ...getColumnSearchProps("TURBIDITY"),
      render: (index) => {
        return <p>{index + " NTU"}</p>;
      },
    },
    {
      title: "status",
      dataIndex: "STATUS",
      key: "STATUS",
      width: "15%",
      ...getColumnSearchProps("STATUS"),
      render: (index) => {
        return <StatusComponent color={index}>{index}</StatusComponent>;
      },
    },
  ];

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  return (
    <Transitions>
      <LayoutMenu header={"Turbidity"}>
        <div className="mx-5 drop-shadow-lg">
          
          <div className="grid grid-cols-3 gap-x-[30px]">
            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WHAT IS TURBIDITY
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Turbidity is a measure of the cloudiness or haziness of a
                  liquid caused by the presence of suspended particles, such as
                  sediment, algae, or other microscopic organisms.
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  onClick={() => {
                    setModalOpen1(true);
                  }}
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  STANDARD TURBIDITY
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  The water turbidity standard is set between 5 to 25 NTU
                  (Nephelometric Turbidity Unit) and if it exceeds the set limit
                  it will cause: 1. Disturbing aesthetics 2. Reducing the
                  effectiveness of water disinfection.
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  onClick={() => setModalOpen2(true)}
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                >
                  Read More
                </Button>
              </div>
            </Card>

            <Card className="px-[20px] py-[20px]">
              <div className="flex justify-center h-full">
                <p className="font-bold text-center text-[14px] text-[#FE634E]">
                  WATER TURBIDITY REGULATION
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <p className="font-normal text-center text-[12px] text-[#000000]">
                  Water turbidity regulation refers to the process of
                  controlling and maintaining the clarity of water by reducing
                  the amount of suspended particles and solids in it. Turbidity
                  is a measure of the cloudiness or haziness of water caused by
                  the presence of these particles
                </p>
              </div>
              <div className="flex justify-center h-full mt-[10px]">
                <Button
                  onClick={() => {
                    setModalOpen3(true);
                  }}
                  className="flex justify-center rounded-full bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                >
                  Read More
                </Button>
              </div>
            </Card>
          </div>

          <div className="mt-[30px]">
            <Card>
              <div>
                <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                  turbidity history
                </p>
              </div>
              <div className="mt-[30px]">
                <Table
                  loading={loading}
                  columns={columns}
                  dataSource={data}
                  scroll={{
                    x: "max-content",
                  }}
                />
              </div>
            </Card>
          </div>

          {/* Modal Card 1 */}
          <Modal
            title="What is Turbidity"
            open={modalOpen1}
            onOk={false}
            onCancel={() => setModalOpen1(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen1(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={274}
              src={Turbidity1}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Turbidity is a measure of the cloudiness or haziness of a liquid
              caused by the presence of suspended particles, such as sediment,
              algae, or other microscopic organisms. The more suspended
              particles in the liquid, the higher its turbidity. Turbidity is
              measured in nephelometric turbidity units (NTUs) or Jackson
              turbidity units (JTL) and is commonly used as an indicator of
              water quality in environmental monitoring.
            </p>
          </Modal>

          {/* Modal Card 2 */}
          <Modal
            title="Standard Turbidity"
            open={modalOpen2}
            onOk={false}
            onCancel={() => setModalOpen2(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen2(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={274}
              src={Turbidity2}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              The water turbidity standard is set between 5 to 25 NTU
              (Nephelometric Turbidity Unit) and if it exceeds the set limit it
              will cause:
              <br />
              1. Disturbing aesthetics <br />
              2. Reducing the effectiveness of water disinfection <br />
              In terms of drinking water, the higher the level of turbidity, the
              higher that people may get digestive diseases. Especially the
              immune problem, because contaminants such as viruses or bacteria
              can attach to suspended solids.
            </p>
          </Modal>

          {/* Modal Card 3 */}
          <Modal
            title="WATER TURBIDITY REGULATION"
            open={modalOpen3}
            onOk={false}
            onCancel={() => setModalOpen3(false)}
            width={800}
            footer={[
              <>
                <div className="flex justify-end">
                  <Button
                    onClick={() => setModalOpen3(false)}
                    className="flex justify-center rounded-md bg-[#FE634E] px-3 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
                  >
                    Back
                  </Button>
                </div>
              </>,
            ]}
            centered
          >
            <Image
              preview={false}
              width={750}
              height={274}
              src={Turbidity3}
              className={"mt-[10px]"}
            />
            <p className="font-normal text-[15px] text-[#000000] tracking-normal break-word mt-[20px]">
              Water turbidity regulation refers to the process of controlling
              and maintaining the clarity of water by reducing the amount of
              suspended particles and solids in it. Turbidity is a measure of
              the cloudiness or haziness of water caused by the presence of
              these particles, which can come from natural sources such as soil
              erosion or from human activities such as construction or
              wastewater discharge. High turbidity levels in water can make it
              difficult to treat for drinking water purposes, can interfere with
              aquatic life, and can make it less aesthetically pleasing for
              recreational use. To regulate water turbidity, a number of methods
              can be employed, such as: <br />
              1. Sedimentation: Allowing water to sit still so that the heavier
              particles can settle to the bottom and the clearer water can be
              removed from the top. <br />
              2. Filtration: Passing water through filters, such as sand or
              activated carbon, to remove suspended particles. <br />
              3. Coagulation and flocculation: Adding chemicals to water that
              cause suspended particles to clump together and form larger, more
              easily removable particles. <br />
              4. Vegetative buffers: Planting vegetation along streams or other
              water sources to reduce soil erosion and the amount of sediment
              and particles entering the water. <br />
              5. Best management practices: Implementing best management
              practices, such as controlling erosion, minimizing land
              disturbance during construction, and properly managing wastewater
              discharges, can help to reduce the amount of sediment and
              particles entering the water. <br />
              Overall, water turbidity regulation is an important process for
              maintaining the quality and safety of water for various uses, and
              requires a combination of approaches to effectively manage and
              reduce turbidity levels.
            </p>
          </Modal>
        </div>
      </LayoutMenu>
    </Transitions>
  );
};

export default Turbidity;
