import React, { useState, useRef } from "react";
import {
  Card,
  Select,
  Table,
  Input,
  Form,
  DatePicker,
  Button,
  Tag,
} from "antd";
import LayoutMenu from "../../Components/LayoutMenu";
import StatusComponent from "../../Components/StatusComponent";
import moment from "moment";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import PDF from "../../Components/PDF";
import XLSX from "../../Components/XLSX";

const initialBody = {
  startDate: "",
  endDate: "",
  status: "",
};

const tagRender = (props) => {
  const { label, closable, onClose } = props;
  const onPreventMouseDown = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };
  return (
    <Tag
      color={"fe634e"}
      onMouseDown={onPreventMouseDown}
      closable={closable}
      onClose={onClose}
      style={{
        marginRight: 3,
      }}
    >
      {label}
    </Tag>
  );
};

const Report = () => {
  const { Option } = Select;
  const searchInput = useRef(null);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  // const [startDate, setStartDate] = useState("");
  // const [endDate, setEndDate] = useState("");
  // const [status, setStatus] = useState("");
  const [{ startDate, endDate, status }, setState] = useState(initialBody);
  const [optionsSelected, setOptionsSelected] = useState([]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const columns = [
    {
      title: "no",
      dataIndex: "no",
      key: "no",
      align: "center",
    },
    {
      title: "date",
      dataIndex: "date",
      key: "date",
      align: "center",
      ...getColumnSearchProps("date"),
      render: (index) => {
        return moment(index).format("DD-MMM-YYYY HH:MM:SS");
      },
    },
    {
      title: "duration",
      dataIndex: "duration",
      key: "duration",
      align: "center",
      ...getColumnSearchProps("duration"),
    },
    {
      title: "total dissolve solid",
      dataIndex: "totalDissolveSolid",
      key: "totalDissolveSolid",
      align: "center",
      ...getColumnSearchProps("totalDissolveSolid"),
    },
    {
      title: "acidity",
      dataIndex: "acidity",
      key: "acidity",
      align: "center",
      ...getColumnSearchProps("acidity"),
    },
    {
      title: "turbidity",
      dataIndex: "turbidity",
      key: "turbidity",
      align: "center",
      ...getColumnSearchProps("turbidity"),
    },
    {
      title: "temperature",
      dataIndex: "temperature",
      key: "temperature",
      align: "center",
      ...getColumnSearchProps("temperature"),
    },
    {
      title: "status",
      dataIndex: "status",
      key: "status",
      ...getColumnSearchProps("status"),
      render: (index) => {
        return (
          <StatusComponent color={index === "GOOD" ? "green" : "yellow"}>
            {index}
          </StatusComponent>
        );
      },
    },
  ];

  const data = [];
  for (let i = 0; i < 100; i++) {
    data.push({
      key: i,
      no: `${i + 1}`,
      date: `22 Jan 2023 23:11:09`,
      duration: `Ongoing for ${i} minutes`,
      totalDissolveSolid: `${i} PPM`,
      acidity: `${i} pH`,
      turbidity: `${i} NTU`,
      temperature: `${i} C`,
      status: "GOOD",
    });
  }

  const [kolom, setKolom] = useState(columns);

  const handleChange = (value) => {
    setKolom(() =>
      columns.filter((x) => {
        return !value.includes(x.title);
      })
    );
    setOptionsSelected(value);
  };

  const search = () => {
    const body = {
      startDate: startDate,
      endDate: endDate,
      status: status,
    };
    console.log({ body });
  };

  console.log(columns.filter((x) => x.title).splice(1));

  return (
    <LayoutMenu header={"Report"}>
      <div className="mx-5 drop-shadow-lg">
        <Card>
          <div>
            <p className="font-bold text-[14px] text-[#FE634E] uppercase">
              filter reporting
            </p>
          </div>
          <Form
            name="customized_form_controls"
            layout="vertical"
            className="w-full grid grid-cols-3 gap-x-3"
          >
            <Form.Item label="Start Date" className="flex-col my-1">
              <DatePicker
                style={{ width: "100%" }}
                allowClear
                onChange={(date) => {
                  setState((prevState) => ({
                    ...prevState,
                    startDate: date,
                  }));
                }}
              />
            </Form.Item>

            <Form.Item label="End Date" className="flex-col my-1">
              <DatePicker
                allowClear
                style={{ width: "100%" }}
                onChange={(date) =>
                  setState((prevState) => ({ ...prevState, endDate: date }))
                }
                disabledDate={(current) => {
                  let customDate = moment(startDate);
                  return current && current < moment(customDate, "YYYY-MM-DD");
                }}
              />
            </Form.Item>

            <Form.Item label="Status" className="flex-col my-1">
              <Select
                showSearch
                defaultValue={"Select Status"}
                allowClear
                onChange={(e) =>
                  setState((prevState) => ({ ...prevState, status: e }))
                }
                value={status}
                options={[
                  {
                    value: "1",
                    label: "Very Good",
                  },
                  {
                    value: "2",
                    label: "Good",
                  },
                  {
                    value: "3",
                    label: "Bad",
                  },
                  {
                    value: "4",
                    label: "Very Bad",
                  },
                  {
                    value: "5",
                    label: "Warning",
                  },
                ]}
              />
            </Form.Item>
          </Form>
          <div className="my-4 mx-4 flex justify-end">
            <Button
              className="inline-flex items-center justify-center rounded-lg bg-[#FE634E] px-5 py-1.5 text-sm font-semibold text-white hover:bg-[#FE634E] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#FE634E]"
              onClick={() => {
                search();
              }}
            >
              Search
            </Button>
          </div>
        </Card>
        <div className="mt-[30px]">
          <Card>
            <div className="mb-4">
              <p className="font-bold text-[14px] text-[#FE634E] uppercase">
                reporting history
              </p>
            </div>
            <div className="flex justify-between">
              <Select
                mode="multiple"
                placeholder="Show/Hide column"
                allowClear
                style={{ minWidth: "300px" }}
                size="large"
                onChange={handleChange}
                maxTagCount={3}
                tagRender={tagRender}
              >
                {columns
                  .map((x) => (
                    <Option
                      disabled={
                        optionsSelected.length > 2
                          ? optionsSelected.includes(x.title)
                            ? false
                            : true
                          : false
                      }
                      value={x.title}
                    >
                      {x.title}
                    </Option>
                  ))
                  .splice(1)}
              </Select>
              <div className="flex">
                <PDF data={data} columns={kolom} />
                <XLSX data={data} columns={kolom} />
              </div>
            </div>
            <div className="mt-[30px]">
              <Table
                columns={kolom}
                dataSource={data}
                scroll={{
                  x: "max-content",
                }}
              />
            </div>
          </Card>
        </div>
      </div>
    </LayoutMenu>
  );
};

export default Report;
