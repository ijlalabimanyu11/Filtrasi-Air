import axios from "axios";
import { ConfigApp } from "../../Contants/ConfigApp";
import { tokenHeader } from "../../Utils/tokenHeader";

const login = async (user) => {
  const response = await axios.post(`${ConfigApp.SERVICE}users/login`, user);
  return response.data;
};

const logout = async () => {
  const response = await axios.post(`${ConfigApp.SERVICE}users/logout`);
  localStorage.clear();
  window.sessionStorage.clear();
  return response.data;
};

const authService = {
  login,
  logout,
};

export default authService;
