import axios from "axios";
import { ConfigApp } from "../../Contants/ConfigApp";

const getDataRealTime = async () => {
  const response = await axios.get(
    `${ConfigApp.SERVICE}filtration/data-result`
  );
  return response.data;
};

const realTimeService = {
  getDataRealTime,
};


export default realTimeService