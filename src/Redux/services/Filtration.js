import axios from "axios";
import { ConfigApp } from "../../Contants/ConfigApp";

const getTDS = async () => {
  const response = await axios.get(
    `${ConfigApp.SERVICE}filtration/total-dissolve-solid`
  );
  return response.data;
};

const getAcidity = async () => {
  const response = await axios.get(`${ConfigApp.SERVICE}filtration/acidity`);
  return response.data;
};

const getTurbidity = async () => {
  const response = await axios.get(`${ConfigApp.SERVICE}filtration/turbidity`);
  return response.data;
};

const getTemperature = async () => {
  const response = await axios.get(
    `${ConfigApp.SERVICE}filtration/temperature`
  );
  return response.data;
};

const getDataResult = async () => {
  const response = await axios.get(
    `${ConfigApp.SERVICE}filtration/data-result`
  );
  return response.data;
};

const getDataWeekly = async (body) => {
  const response = await axios.post(
    `${ConfigApp.SERVICE}filtration/data-weekly`,
    body
  );
  return response.data;
};

const getMonthly = async (body) => {
  const response = await axios.post(
    `${ConfigApp.SERVICE}filtration/data-monthly`,
    body
  );
  return response.data;
};
const filtrationService = {
  getTDS,
  getAcidity,
  getTurbidity,
  getTemperature,
  getDataResult,
  getDataWeekly,
  getMonthly,
};

export default filtrationService;
