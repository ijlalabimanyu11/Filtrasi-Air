import axios from "axios";
import { ConfigApp } from "../../Contants/ConfigApp";

const register = async (body) => {
  const response = await axios.post(`${ConfigApp.SERVICE}users/register`, body);
  return response.data;
};

const forgotPassword = async (body) => {
  const response = await axios.post(`${ConfigApp.SERVICE}users/forgot-password`, body);
  return response.data;
};

const newPassword = async (body) => {
  const response = await axios.post(`${ConfigApp.SERVICE}users/new-password`, body);
  return response.data;
};


const usersServices = {
  register,
  forgotPassword,
  newPassword
};

export default usersServices;
