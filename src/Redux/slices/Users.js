import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import usersServices from "../services/Users";

const initialState = {
  isLoggedIn: false,
  data: null,
  failedRequest: false,
  message: "",
  loading: false,
};

export const registerSlice = createAsyncThunk(
  "REGISTER",
  async (body, thunkAPI) => {
    try {
      const response = await usersServices.register(body);
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const forgotPasswordSlice = createAsyncThunk(
  "FORGOT_PASSWORD",
  async (body, thunkAPI) => {
    try {
      const response = await usersServices.forgotPassword(body);
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const newPasswordSlice = createAsyncThunk(
  "NEW_PASSWORD",
  async (body, thunkAPI) => {
    try {
      const response = await usersServices.newPassword(body);
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const usersSlice = createSlice({
  name: "users",
  initialState,
  extraReducers: {
    // Register
    [registerSlice.pending]: (state) => {
      state.loading = true;
      state.isLoggedIn = false;
      state.failedRequest = false;
    },
    [registerSlice.fulfilled]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = true;
      state.failedRequest = false;
      state.data = action.payload;
    },
    [registerSlice.rejected]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = false;
      state.failedRequest = true;
      state.data = action.payload;
    },

    // Forgot Password
    [forgotPasswordSlice.pending]: (state) => {
      state.loading = true;
      state.isLoggedIn = false;
      state.failedRequest = false;
    },
    [forgotPasswordSlice.fulfilled]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = true;
      state.failedRequest = false;
      state.data = action.payload;
    },
    [forgotPasswordSlice.rejected]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = false;
      state.failedRequest = true;
      state.data = action.payload;
    },

    // New Password
    [newPasswordSlice.pending]: (state) => {
      state.loading = true;
      state.isLoggedIn = false;
      state.failedRequest = false;
    },
    [newPasswordSlice.fulfilled]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = true;
      state.failedRequest = false;
      state.data = action.payload;
    },
    [newPasswordSlice.rejected]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = false;
      state.failedRequest = true;
      state.data = action.payload;
    },
  },
});

const { reducer } = usersSlice;
export default reducer;
