import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import filtrationService from "../services/Filtration";

const initialState = {
  data: [],
  dataWeekly: [],
  dataMonthly: [],
  data_detail: {},
  isFailed: false,
  isSuccess: false,
  message: "",
  loading: false,
};

export const getSliceTDS = createAsyncThunk(
  "GET_ALL_LIST_TDS",
  async (thunkAPI) => {
    try {
      const response = await filtrationService.getTDS();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

export const getSliceAcidity = createAsyncThunk(
  "GET_ALL_LIST_ACIDITY",
  async (thunkAPI) => {
    try {
      const response = await filtrationService.getAcidity();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

export const getSliceTurbidity = createAsyncThunk(
  "GET_ALL_LIST_TURBIDITY",
  async (thunkAPI) => {
    try {
      const response = await filtrationService.getTurbidity();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

export const getSliceTemperature = createAsyncThunk(
  "GET_ALL_LIST_TEMPERATURE",
  async (thunkAPI) => {
    try {
      const response = await filtrationService.getTemperature();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

export const getSliceDataResult = createAsyncThunk(
  "GET_ALL_LIST_DATA_RESULT",
  async (thunkAPI) => {
    try {
      const response = await filtrationService.getDataResult();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

export const getSliceDataWeekly = createAsyncThunk(
  "GET_ALL_LIST_DATA_WEEKLY",
  async (body, thunkAPI) => {
    try {
      const response = await filtrationService.getDataWeekly(body);
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.dataWeekly);
    }
  }
);

export const getSliceDataMonthly = createAsyncThunk(
	"GET_ALL_LIST_DATA_MONTHLY",
	async (body, thunkAPI) => {
		try {
			const response = await filtrationService.getMonthly(body);
			return response.data;
		} catch (response) {
			return thunkAPI.rejectWithValue(response.response.dataMonthly);
		}
	}
);


const filtrationSlice = createSlice({
  name: "filtration",
  initialState,
  extraReducers: {
    // get all TDS
    [getSliceTDS.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceTDS.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceTDS.rejected]: (state, action) => {
      state.loading = true;
    },

    // get all Acidity
    [getSliceAcidity.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceAcidity.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceAcidity.rejected]: (state, action) => {
      state.loading = true;
    },

    // get all Turbidity
    [getSliceTurbidity.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceTurbidity.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceTurbidity.rejected]: (state, action) => {
      state.loading = true;
    },

    // get all Temperature
    [getSliceTemperature.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceTemperature.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceTemperature.rejected]: (state, action) => {
      state.loading = true;
    },

    // get all Data Result
    [getSliceDataResult.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceDataResult.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceDataResult.rejected]: (state, action) => {
      state.loading = true;
    },

    // get all Data Weekly
    [getSliceDataWeekly.pending]: (state, action) => {
      state.dataWeekly = action.payload;
      state.loading = true;
    },
    [getSliceDataWeekly.fulfilled]: (state, action) => {
      state.dataWeekly = action.payload;
      state.loading = false;
    },
    [getSliceDataWeekly.rejected]: (state, action) => {
      state.dataWeekly = action.payload;
      state.loading = true;
    },

    // get all Data Monthly
    [getSliceDataMonthly.pending]: (state, action) => {
			state.loading = true;
			state.dataMonthly = action.payload;
		},
		[getSliceDataMonthly.fulfilled]: (state, action) => {
			state.dataMonthly = action.payload;
			state.loading = false;
		},
		[getSliceDataMonthly.rejected]: (state, action) => {
			state.dataMonthly = action.payload;
			state.loading = true;
		},

  },
});

const { reducer } = filtrationSlice;
export default reducer;
