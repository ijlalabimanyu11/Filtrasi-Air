import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import realTimeService from "../services/RealTime";

const initialState = {
  data: [],
  message: "",
  loading: false,
};

export const getSliceRealTIme = createAsyncThunk(
  "GET_ALL_LIST_REAL_TIME",
  async (thunkAPI) => {
    try {
      const response = await realTimeService.getDataRealTime();
      return response.data;
    } catch (response) {
      return thunkAPI.rejectWithValue(response.response.data);
    }
  }
);

const realTimeSlice = createSlice({
  name: "filtration",
  initialState,
  extraReducers: {
    // get all Data Real Time
    [getSliceRealTIme.pending]: (state, action) => {
      state.loading = true;
    },
    [getSliceRealTIme.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getSliceRealTIme.rejected]: (state, action) => {
      state.loading = true;
    },
  },
});


const { reducer } = realTimeSlice;
export default reducer;