import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import authService from "../services/Auth";
import { setData } from "./DataSlice";

const initialState = {
	isLoggedIn: false,
	user: null,
	loading: false,
	failedRequest: false,
	token: localStorage.getItem('token') || window.sessionStorage.getItem('token'),
	isAuthenticated: '',
	remember : false
}


export const login = createAsyncThunk("LOGIN", async ({user, remember}, thunkAPI) => {
	try {
		const data = await authService.login(user);
		if (remember) {
			thunkAPI.dispatch(setData({ key: 'token', data: data.data.token, storageType: "local" }))
		} else {
			thunkAPI.dispatch(setData({ key: 'token', data: data.data.token, storageType: "session" }))
		}
		return data;
	} catch (error) {
		return thunkAPI.rejectWithValue(error);
	}
})

export const logout = createAsyncThunk("LOGOUT", async (_, thunkAPI) => {
  try {
    const data = await authService.logout();
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

const authSlice = createSlice({
  name: "auth",
  initialState,
  extraReducers: {
    [login.pending]: (state, action) => {
      state.loading = true;
      state.isLoggedIn = false;
      state.failedRequest = false;
    },
    [login.fulfilled]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = true;
      state.failedRequest = false;
      state.user = action.payload;
      state.token =
        localStorage.getItem("token") || window.sessionStorage.getItem("token");
    },
    [login.rejected]: (state, action) => {
      state.loading = false;
      state.isLoggedIn = false;
      state.user = action.payload;
      state.failedRequest = true;
    },
    [logout.pending]: (state, action) => {
      state.isLoggedIn = true;
      state.loading = true;
    },
    [logout.fulfilled]: (state, action) => {
      state.isLoggedIn = false;
      state.loading = false;
      state.user = action.payload;
    },
    [logout.rejected]: (state, action) => {
      state.isLoggedIn = true;
      state.loading = false;
      state.failedRequest = true;
      state.user = action.payload;
    },
  },
});

const { reducer } = authSlice;
export default reducer;
