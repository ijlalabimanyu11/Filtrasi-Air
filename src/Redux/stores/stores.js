import { configureStore } from "@reduxjs/toolkit";
import filtrationReducer from "../slices/Filtration"
import authReducer from "../slices/Auth"
import dataReducer from "../slices/DataSlice"
import usersReducer from "../slices/Users"
import realTimeReducer from "../slices/RealTime"

const reducer = {	
	auth: authReducer,
	filtration: filtrationReducer,
	dataSlice: dataReducer,
	user: usersReducer,
	realTime: realTimeReducer
};

const store = configureStore({
	reducer: reducer,
	devTools: true,
});

export default store;
